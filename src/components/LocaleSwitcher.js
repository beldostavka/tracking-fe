import React from 'react'
import {MenuItem, NavDropdown} from 'react-bootstrap';

import '../components/Home.css';

export default class LocaleSwitcher extends React.Component {
    onSelectLanguage = (language) => {
        this.props.onChangeLanguage(language);
    };

    render() {
        const {language} = this.props;

        return (
            <NavDropdown title={`${language.toUpperCase()}`} id="locale-switcher" onSelect={this.onSelectLanguage}>
                <MenuItem eventKey="ru">Русский</MenuItem>
                <MenuItem eventKey="en">English</MenuItem>
            </NavDropdown>
        )
    }
}
