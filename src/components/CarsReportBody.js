import React, {Component} from "react";

import {Table} from "react-bootstrap";


export default class CarsReportBody extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let data = this.props.reportBody;

        let tHeadTemplate;
        let tBodyTemplate;
        let keys;

        function strTable(i) {
            let stringTable;
            let repObject = data[i];

            let arrayFromObject = Object.values(repObject);

            stringTable = arrayFromObject.map(function (item, index) {
                return (<td key={index}>{item}</td>);
            });
            console.log(stringTable);
            return stringTable;

        }

        if (data.length > 0) {
            keys = Object.keys(data[0]);
            tHeadTemplate = keys.map(function (item, index) {
                return (
                    <th key={index}>{item}</th>
                )
            })
        }

        if (data.length > 0) {

            tBodyTemplate = data.map(function (item, index) {
                return (
                    <tr key={index}>
                        {strTable(index)}
                    </tr>
                )
            })
        }

        return (
            <div>
                <Table striped bordered condensed hover>
                    <thead>
                    <tr>
                        {tHeadTemplate}
                    </tr>
                    </thead>
                    <tbody>
                    {tBodyTemplate}
                    </tbody>
                </Table>
            </div>
        )
    }
}
