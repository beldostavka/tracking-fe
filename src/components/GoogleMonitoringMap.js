import React from 'react';
import { withGoogleMap, GoogleMap } from "react-google-maps";
import CarMarker from "./CarMarker";
import {APP_CONFIG} from "../config/index";

const WrappedMap = withGoogleMap(props => (
    <GoogleMap
        ref={props.onMapLoad}
        defaultZoom={APP_CONFIG.monitoring.defaultZoom}
        defaultCenter={{ lat: 53.901311, lng: 27.558996 }}
        options={{
            maxZoom: APP_CONFIG.monitoring.maxZoom
        }}
        onZoomChanged={props.onZoomChanged}
    >
        {props.children}
    </GoogleMap>
));

const google = window.google;

const COORDINATE_DELTA = 0.001;

export default class GoogleMonitoringMap extends React.Component {
    mapInstance;

    constructor(props) {
        super(props);

        this.state = {
            zoom: APP_CONFIG.monitoring.defaultZoom
        }
    }

    componentWillReceiveProps(nextProps) {
        const nextZoomCarId = nextProps.zoomCarId;

        let coordinatesToZoom = nextProps.latestCoordinates;

        // If we would like to track this particular car just use it's coordinates
        if (nextZoomCarId && Array.isArray(coordinatesToZoom[nextZoomCarId]) && coordinatesToZoom[nextZoomCarId].length > 0) {
            coordinatesToZoom = {
                [nextZoomCarId]: coordinatesToZoom[nextZoomCarId],
                // This is workaround for only one coordinate to zoom
                "DUMMY_ID_LEFT": [{
                    lattitude: coordinatesToZoom[nextZoomCarId][0].lattitude - COORDINATE_DELTA,
                    longitude: coordinatesToZoom[nextZoomCarId][0].longitude + COORDINATE_DELTA
                }],
                "DUMMY_ID_RIGHT": [{
                    lattitude: coordinatesToZoom[nextZoomCarId][0].lattitude + COORDINATE_DELTA,
                    longitude: coordinatesToZoom[nextZoomCarId][0].longitude - COORDINATE_DELTA
                }]
            };
        }

        this.fitMapToMarkers(coordinatesToZoom);
    }

    convertCoordinatesToMarkers(coordinates = {}, carsGeocode = {}, cars = []) {
        let coordinate,
            latestCoordinates,
            geocode,
            markers,
            carId;

        markers = cars.map(car => {
            carId = car.id;

            latestCoordinates = coordinates[carId] || null;

            geocode = carsGeocode && carsGeocode[carId] || null;

            if (Array.isArray(latestCoordinates) && (coordinate = latestCoordinates[latestCoordinates.length - 1])) {
                return <CarMarker
                    coordinate={coordinate}
                    car={car}
                    key={carId}
                    onCarClick={this.props.onCarClick}
                    geocode={geocode}
                    onGeoCodeClose={this.props.onGeoCodeClose}
                    zoom={this.state.zoom}
                />;
            }

            return null;
        });

        return markers.filter(marker => marker !== null);
    }

    fitMapToMarkers = carsCoordinates => {
        if (!this.mapInstance || !carsCoordinates) {
            return;
        }

        let carId,
            bounds,
            latLng = [],
            carCoordinates,
            latestCarCoordinate;

        for (carId in carsCoordinates) {
            if (!carsCoordinates.hasOwnProperty(carId)) {
                continue;
            }

            carCoordinates = carsCoordinates[carId] || null;

            if (Array.isArray(carCoordinates) && (latestCarCoordinate = carCoordinates[carCoordinates.length - 1])) {
                latLng.push(
                    new google.maps.LatLng(latestCarCoordinate.lattitude, latestCarCoordinate.longitude)
                );
            }
        }

        if (latLng.length === 0) {
            return;
        }

        bounds = new google.maps.LatLngBounds();

        latLng.forEach(coordinate => {
            bounds.extend(coordinate);
        });

        this.mapInstance.fitBounds(bounds);
    };

    onMapLoad = map => {
        this.mapInstance = map;
    };

    render() {
        const {latestCoordinates, carsGeocode, cars} = this.props;

        return (
            <WrappedMap
                containerElement={<div style={{ height: `100%` }} />}
                mapElement={<div style={{ height: `100%` }} />}
                onMapLoad={this.onMapLoad}
                onZoomChanged={() => {
                    this.setState({
                        zoom: this.mapInstance.getZoom()
                    });
                }}
            >
                {this.convertCoordinatesToMarkers(latestCoordinates, carsGeocode, cars)}
            </WrappedMap>
        );
    };
}
