import * as React from "react";
import {OverlayView, Polygon} from "react-google-maps";

const google = window.google;

export default class Zone extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {zone: {id, name, bounds, color}} = this.props;

        let googleBounds = new google.maps.LatLngBounds();

        bounds.forEach(bound => {
            googleBounds.extend(new google.maps.LatLng(bound));
        });

        return (
            <div>
                <Polygon
                    paths={bounds}
                    visible={true}
                    key={id}
                    options={{
                        strokeColor: color,
                        fillColor: color
                    }}
                />
                <OverlayView
                    position={googleBounds.getCenter()}
                    mapPaneName={OverlayView.OVERLAY_LAYER}
                    getPixelPositionOffset={(width, height) => ({
                        x: -(width / 2),
                        y: -(height / 2),
                    })}
                >
                    <p style={{ fontSize: `1.25em`, color: `${color}`, }}>
                        {name}
                    </p>
                </OverlayView>
            </div>
        );
    }
}
