import * as React from "react";
import {Button, ButtonGroup, ButtonToolbar, Col, Glyphicon, ProgressBar, Row} from "react-bootstrap";
import "../styles/TrackPlayer.css";
import {APP_CONFIG} from "../config/index";

const google = window.google;

export default class TripsPlayer extends React.Component {
    coordinatesLength;

    constructor(props) {
        super(props);

        this.state = {
            play: false,
            acceleration: 1,
            playHead: 0
        };
    }

    componentDidMount() {
        if (this.coordinatesLength !== this.props.coordinates.length) {
            this.coordinatesLength = this.props.coordinates.length - 1;
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.coordinatesLength !== nextProps.coordinates.length) {
            this.coordinatesLength = nextProps.coordinates.length - 1;
        }
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    play = () => {
        const coordinatesLength = this.coordinatesLength;

        this.setState((prevState, props) => ({
            playHead: prevState.playHead < coordinatesLength ? ++prevState.playHead : coordinatesLength
        }));

        this.props.onPlayheadUpdate(this.props.car.id, this.props.coordinates[this.state.playHead]);
    };

    onPlayClick = () => {
        this.setState({
            play: true
        });

        this.startPlayback();
    };

    startPlayback = () => {
        this.timer = setInterval(
            this.play,
            this.getPlaybackSpeed()
        );
    };

    getPlaybackSpeed = () => {
        return APP_CONFIG.player.speed / this.state.acceleration;
    };

    onPauseClick = () => {
        clearInterval(this.timer);

        this.setState({
            play: false
        });
    };

    onBackwardClick = () => {
        this.setState((prevState, props) => ({
            acceleration: prevState.acceleration > 1 ? --prevState.acceleration : 1
        }));

        if (this.state.play) {
            this.updateSpeed();
        }
    };

    onForwardClick = () => {
        this.setState((prevState, props) => ({
            acceleration: ++prevState.acceleration
        }));

        if (this.state.play) {
            this.updateSpeed();
        }
    };

    onStopClick = () => {
        this.onPauseClick();

        this.setState((prevState, props) => ({
            playHead: 0
        }));

        this.props.onPlayheadUpdate(this.props.car.id, this.props.coordinates[this.state.playHead]);
    };

    onProgressClick = event => {
        event.preventDefault();

        const element = event.currentTarget;

        let progress = (event.clientX - element.offsetLeft) / element.clientWidth;

        progress = isNaN(progress) ? 0 : progress;

        const newPlayHead = Math.round(this.coordinatesLength * progress);

        this.setState((prevState, props) => ({
            playHead: newPlayHead
        }));
    };

    onCloseClick = () => {
        this.onPauseClick();

        this.props.onClose(this.props.car.id);
    };

    updateSpeed = () => {
        clearInterval(this.timer);

        this.startPlayback();
    };

    getProgress = () => {
        return Math.round((this.state.playHead / this.coordinatesLength) * 100);
    };

    render() {
        const {car} = this.props;

        let playPause;

        if (this.state.play) {
            playPause = <Button bsSize="small" onClick={this.onPauseClick}><Glyphicon glyph="pause"/></Button>;
        } else {
            playPause = <Button bsSize="small" onClick={this.onPlayClick}><Glyphicon glyph="play"/></Button>;
        }
        return (
            <Row className="show-grid">
                <Col sm={12} md={12}>
                    <div className="track-player">
                        <div className="player-top">
                            <div className="player-controls">
                                <ButtonToolbar>
                                    <ButtonGroup>
                                        <Button bsSize="small" onClick={this.onBackwardClick}>
                                            <Glyphicon glyph="backward" />
                                        </Button>
                                        {playPause}
                                        <Button bsSize="small" onClick={this.onStopClick}>
                                            <Glyphicon glyph="stop" />
                                        </Button>
                                        <Button bsSize="small" onClick={this.onForwardClick}>
                                            <Glyphicon glyph="forward" />
                                        </Button>
                                    </ButtonGroup>
                                </ButtonToolbar>
                                <p className="player-acceleration">{this.state.acceleration}x</p>
                            </div>
                            <div className="track-info">
                                <p>{car.name}</p>
                                <div style={{backgroundColor: car.color}} className="track-color" />
                            </div>
                            <div>
                                <ButtonToolbar>
                                    <ButtonGroup>
                                        <Button bsSize="small" onClick={this.onCloseClick}>
                                            <Glyphicon glyph="remove" />
                                        </Button>
                                    </ButtonGroup>
                                </ButtonToolbar>
                            </div>
                        </div>
                        <div className="player-bottom" onClick={this.onProgressClick}>
                            <ProgressBar now={this.getProgress()} />
                        </div>
                    </div>
                </Col>
            </Row>
        );
    }
}
