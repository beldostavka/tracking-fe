import * as React from "react";
import CarTrip from "./CarTrip";
import CarPositionInfo from "./CarPositionInfo";
import moment from "moment";
import {APP_CONFIG} from "../config/index";
import {translate} from "react-i18next";

class CarTrips extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {car: {id, color, name}, trackGeocode, settings, tripsCoordinates, onTripClick, t} = this.props;

        const latitude = trackGeocode
                && trackGeocode.coordinate
                && trackGeocode.coordinate.latitude
                || null,
            longitude = trackGeocode
                && trackGeocode.coordinate
                && trackGeocode.coordinate.longitude
                || null,
            time = trackGeocode
                && trackGeocode.coordinate
                && trackGeocode.coordinate.time
                || null,
            parsedTime = Number.isFinite(time) ? moment(time).format(APP_CONFIG.time.format) : t('could_not_get_time');

        const trips = tripsCoordinates.map((tripCoordinates, index) => {
            return <CarTrip
                coordinates={tripCoordinates}
                onTripClick={onTripClick}
                key={index}
                carId={id}
                settings={settings}
                color={color}
            />;
        });

        return (
            <div>
                {trips}
                {trackGeocode &&
                    <CarPositionInfo
                        onClose={() => this.props.onGeoCodeClose(id)}
                        carName={name}
                        latitude={parseFloat(latitude)}
                        longitude={parseFloat(longitude)}
                        address={trackGeocode.address}
                        time={parsedTime}
                    />
                }
            </div>
        );
    }
}

export default translate('trips', {wait: true})(CarTrips);
