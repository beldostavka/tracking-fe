import React from 'react';
import {Accordion, Panel} from "react-bootstrap";
import GroupSettingsForm from "./GroupSettingsForm";

export default class CarsGroupsEditableList extends React.Component {
    static FORM_NAME = "groupSettings";

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Accordion>
                {this.props.groups.map(group => {
                    let id = group.id;

                    return <Panel header={group.name} eventKey={id} key={id}>
                        <GroupSettingsForm
                            form={`${CarsGroupsEditableList.FORM_NAME}-${id}`}
                            initialValues={group}
                            onUpdate={this.props.onUpdate}
                            onDelete={this.props.onDelete}
                        />
                    </Panel>
                })}
            </Accordion>
        );
    }
}
