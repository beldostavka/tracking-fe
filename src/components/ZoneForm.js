import React from 'react';
import {Alert, Button, ButtonToolbar, Col, Form, FormGroup, Glyphicon} from "react-bootstrap";
import "../styles/CarSettings.css";
import {Field, reduxForm} from "redux-form";
import {ColorPickerField, TextField} from "./FormFields";
import {required} from "../utils/validators";
import {translate} from "react-i18next";

class ZoneForm extends React.Component {
    static NAME = "geoZone";

    constructor(props) {
        super(props);
    }

    onSubmit = data => {
        const {initialValues, onUpdate, onCreate} = this.props;

        if (initialValues && initialValues.id) {
            onUpdate(initialValues.id, data);
        } else {
            onCreate(data);
        }
    };

    boundsFormatter = bounds => {
        const {t} = this.props;

        const latShort = t('monitoring:latitude_short');
        const lngShort = t('monitoring:longitude_short');

        if (Array.isArray(bounds)) {
            return bounds
                .map(bound => `${latShort}: ${bound.lat}, ${lngShort}: ${bound.lng}`)
                .join(", ");
        } else if (bounds) {
            return bounds;
        } else {
            return '';
        }
    };

    onDelete = () => {
        this.props.onDelete(this.props.initialValues.id);
    };

    render() {
        const {initialValues, error, handleSubmit, submitSucceeded, pristine, submitting, t} = this.props;

        return (
            <Form horizontal onSubmit={handleSubmit(this.onSubmit)}>
                {error &&
                <Alert bsStyle="danger">
                    <strong>{t('forms:error_occurred')}!</strong> {error}
                </Alert>
                }
                {submitSucceeded && <Alert bsStyle="success">{t('forms:save_success')}</Alert>}
                <div className="geo-zone-form">
                    <Field
                        name="name"
                        component={TextField}
                        type="text"
                        label={t('labels:name')}
                        validate={required}
                    />
                    <Field
                        name="bounds"
                        component={TextField}
                        type="text"
                        label={t('bounds')}
                        validate={required}
                        format={this.boundsFormatter}
                        disabled={true}
                    />
                    <Field
                        name="speed"
                        component={TextField}
                        type="number"
                        label={t('speed_limit')}
                    />
                    <Field
                        name="color"
                        component={ColorPickerField}
                        label={t('color')}
                    />
                </div>
                <FormGroup>
                    <Col sm={10} smOffset={2}>
                        <ButtonToolbar>
                            <Button type="submit" disabled={pristine || submitting}>
                                {t('forms:save')}
                            </Button>
                            {initialValues && initialValues.id &&
                            <Button
                                bsStyle="danger"
                                onClick={this.onDelete}
                            ><Glyphicon glyph="trash" /></Button>
                            }
                        </ButtonToolbar>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

ZoneForm = reduxForm({form: ZoneForm.NAME})(ZoneForm);

export default translate('zones', {wait: true})(ZoneForm);
