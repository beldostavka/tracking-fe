import React from 'react';
import MonitoringContainer from '../containers/MonitoringContainer';
import {HashRouter, Route} from 'react-router-dom';
import ReportContainer from "../containers/ReportContainer";
import TripsContainer from "../containers/TripsContainer";
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import {APP_CONFIG} from "../config/index";
import {connect} from "react-redux";
import CarSettingsContainer from "../containers/CarSettingsContainer";
import ZonesContainer from "../containers/ZonesContainer";
import EventsContainer from "../containers/EventsContainer";
import {loadEvents} from "../actions/events";
import {bindActionCreators} from "redux";
import { translate } from 'react-i18next';
import LocaleSwitcher from "./LocaleSwitcher";

class Navigation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedItem: 1
        };
    }

    componentDidMount() {
        const {loadEvents} = this.props;

        loadEvents();

        this.timer = setInterval(
            () => loadEvents(),
            APP_CONFIG.events.updateFrequency
        );
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    onSelect = selectedItem => {
        this.setState({
            selectedItem: selectedItem
        });
    };

    buildLogoutUrl = () => {
        const host = APP_CONFIG.application.host;
        const logoutPath = APP_CONFIG.application.urls.logout;

        return `${host}${logoutPath}`;
    };

    render() {
        const {t, i18n, profile: {contact}} = this.props;

        return (
            <HashRouter>
                <div>
                    <Navbar inverse collapseOnSelect>
                        <Navbar.Header>
                            <Navbar.Brand>
                                <a href="/">{t('site_label')}</a>
                            </Navbar.Brand>
                        </Navbar.Header>
                        <Nav activeKey={this.state.selectedItem} onSelect={this.onSelect}>
                            <LinkContainer to="/" key={1} exact={true}>
                                <NavItem eventKey={1} href="/">{t('monitoring')}</NavItem>
                            </LinkContainer>
                            <LinkContainer to="/trips" key={2}>
                                <NavItem eventKey={2} href="/trips">{t('trips')}</NavItem>
                            </LinkContainer>
                            <LinkContainer to="/reports" key={3}>
                                <NavItem eventKey={3} href="/reports">{t('reports')}</NavItem>
                            </LinkContainer>
                            <LinkContainer to="/zones" key={4}>
                                <NavItem eventKey={4} href="/zones">{t('zones')}</NavItem>
                            </LinkContainer>
                            <LinkContainer to="/events" key={5}>
                                <NavItem eventKey={5} href="/events">{t('events')}</NavItem>
                            </LinkContainer>
                            <LinkContainer to="/settings" key={6}>
                                <NavItem eventKey={6} href="/settings">{t('object_settings')}</NavItem>
                            </LinkContainer>
                            <LocaleSwitcher
                                language={i18n.language}
                                onChangeLanguage={language => {
                                    i18n.changeLanguage(language)
                                }}
                            />
                        </Nav>
                        <Navbar.Text pullRight>
                            {contact && `${t('hello')}, ${contact}`}
                            {' '}
                            <Navbar.Link href={this.buildLogoutUrl()}>({t('exit')})</Navbar.Link>
                        </Navbar.Text>
                    </Navbar>
                    <Route exact path="/" component={MonitoringContainer}/>
                    <Route exact path="/trips" component={TripsContainer}/>
                    <Route exact path="/reports" component={ReportContainer}/>
                    <Route exact path="/settings" component={CarSettingsContainer}/>
                    <Route exact path="/zones" component={ZonesContainer}/>
                    <Route exact path="/events" component={EventsContainer}/>
                </div>
            </HashRouter>
        );
    }
}

function mapStateToProps(state) {
    return {
        profile: state.profile || {},
    }
}

function mapDispatchToProps(dispatch) {
    return {
        loadEvents: bindActionCreators(loadEvents, dispatch),
    };
}

Navigation = translate('nav', {wait: true})(Navigation);

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
