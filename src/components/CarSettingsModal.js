import React from 'react';
import {Button, Modal} from "react-bootstrap";
import CarSettingsForm from "./CarSettingsForm";
import {translate} from "react-i18next";

class CarSettingsModal extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {car, groups, iconTypes, fuelTypes, carTypes, onClose, onUpdate, t} = this.props;

        return (
            car &&
            <div className="static-modal">
                <Modal show={car && true} onHide={onClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{`${t('car_settings')} "${car.name}"`}</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <CarSettingsForm
                            initialValues={car}
                            onUpdate={onUpdate}
                            groups={groups}
                            iconTypes={iconTypes}
                            fuelTypes={fuelTypes}
                            carTypes={carTypes}
                        />
                    </Modal.Body>

                    <Modal.Footer>
                        <Button onClick={onClose}>{t('labels:close')}</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default translate('car', {wait: true})(CarSettingsModal);
