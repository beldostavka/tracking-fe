import React from 'react';
import {Button, Glyphicon, Table} from "react-bootstrap";
import {translate} from "react-i18next";

class CarsEventsEditableList extends React.Component {
    static FORM_NAME = "eventSettings";

    constructor(props) {
        super(props);
    }

    onDeleteGenerator = (carEvent) => {
        const {onDelete} = this.props;

        return () => {
            onDelete(carEvent.id);
        };
    };

    render() {
        const {carsEvents, cars, zones, events, t} = this.props;

        return (
            <Table responsive condensed hover>
                <thead>
                    <tr>
                        <th>{t('car')}</th>
                        <th>{t('zone')}</th>
                        <th>{t('event')}</th>
                        <th>{t('actions')}</th>
                    </tr>
                </thead>
                <tbody>
                {carsEvents.map(carEvent => {
                    let id = carEvent.id,
                        car = cars.find(car => car.id === carEvent.vehicle),
                        zone = zones.find(zone => zone.id === carEvent.geozone),
                        event = events.find(event => event.id === carEvent.eventType);

                    if (car && zone && event) {
                        return <tr key={id}>
                            <td>{`${car.name}`}</td>
                            <td>{`${zone.name}`}</td>
                            <td>{`${event.type}`}</td>
                            <td>
                                <Button bsSize="small" bsStyle="danger" onClick={this.onDeleteGenerator(carEvent)}>
                                    <Glyphicon glyph="trash" />
                                </Button>
                            </td>
                        </tr>;
                    }

                    return null;
                })}
                </tbody>
            </Table>
        );
    }
}

export default translate('labels', {wait: true})(CarsEventsEditableList);
