import React, {Component} from "react";
import "./App.css";
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-daterangepicker/daterangepicker.css';
import Navigation from "./Navigation";
import {Grid} from "react-bootstrap";

class App extends Component {
    render() {
        return (
            <Grid fluid={true}>
                <div className="App">
                    <Navigation/>
                </div>
            </Grid>
        );
    }
}

export default App;
