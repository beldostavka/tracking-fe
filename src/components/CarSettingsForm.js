import React from 'react';
import {Alert, Button, Col, Form, FormGroup, Tab, Tabs} from "react-bootstrap";
import "../styles/CarSettings.css";
import {Field, formValueSelector, reduxForm} from "redux-form";
import {ColorPickerField, IconSelectField, MultipleSelectField, SelectField, TextField} from "./FormFields";
import {required} from "../utils/validators";
import {connect} from "react-redux";
import {translate} from "react-i18next";

const NAME = "carSettings";

class CarSettingsForm extends React.Component {
    constructor(props) {
        super(props);
    }

    onSubmit = data => {
        const {initialValues} = this.props;

        this.props.onUpdate(initialValues.id, data);
    };

    listFormatter = value => {
        if (Array.isArray(value)) {
            return value.map(item => item.id);
        } else if (value) {
            return value.id;
        } else {
            return [];
        }
    };

    listNormalizer = value => {
        return formattedList => {
            if (Array.isArray(formattedList)) {
                return value.filter(item => formattedList.indexOf(item.id) > -1);
            } else {
                return value.find(item => formattedList === item.id);
            }
        };
    };

    render() {
        const {
            groups,
            iconTypes,
            fuelTypes,
            carTypes,
            initialValues,
            error,
            handleSubmit,
            submitSucceeded,
            color,
            t
        } = this.props;

        return (
            initialValues &&
            <Form horizontal onSubmit={handleSubmit(this.onSubmit)}>
                {error && <Alert bsStyle="danger">{error}</Alert>}
                {submitSucceeded && <Alert bsStyle="success">{t('forms:save_success')}</Alert>}

                <Tabs defaultActiveKey={1} id="settings-forms">
                    <Tab eventKey={1} title={t('characteristics')}>
                        <div className="car-settings-form">
                            <fieldset>
                                <legend>{t('common_settings')}</legend>
                                <Field
                                    name="vin"
                                    component={TextField}
                                    type="text"
                                    label="VIN"
                                />
                                <Field
                                    name="type"
                                    component={SelectField}
                                    options={carTypes.map(carType => {
                                        return {
                                            name: carType.type,
                                            value: carType.id
                                        };
                                    })}
                                    format={this.listFormatter}
                                    normalize={this.listNormalizer(carTypes)}
                                    label={t('vehicle_type')}
                                />
                                <Field
                                    name="registrNumber"
                                    component={TextField}
                                    type="text"
                                    label={t('registration_number')}
                                />
                                <Field
                                    name="model"
                                    component={TextField}
                                    type="text"
                                    label={t('model')}
                                />
                            </fieldset>
                            <fieldset>
                                <legend>{t('engine_settings')}</legend>
                                <Field
                                    name="engineVolume"
                                    component={TextField}
                                    type="number"
                                    label={t('engine_volume')}
                                />
                                <Field
                                    name="fuelType"
                                    component={SelectField}
                                    options={fuelTypes.map(fuelType => {
                                        return {
                                            name: fuelType.type,
                                            value: fuelType.id
                                        };
                                    })}
                                    format={this.listFormatter}
                                    normalize={this.listNormalizer(fuelTypes)}
                                    label={t('fuel_type')}
                                />
                            </fieldset>
                            <fieldset>
                                <legend>{t('load_settings')}</legend>
                                <Field
                                    name="loadCapacityTonnage"
                                    component={TextField}
                                    type="number"
                                    label={t('load_capacity')}
                                />
                                <Field
                                    name="volume"
                                    component={TextField}
                                    type="number"
                                    label={t('net_volume')}
                                />
                            </fieldset>
                            <fieldset>
                                <legend>{t('car_settings')}</legend>
                                <Field
                                    name="kilometrage"
                                    component={TextField}
                                    type="number"
                                    label={t('mileage')}
                                />
                                <Field
                                    name="fuelMileage"
                                    component={TextField}
                                    type="number"
                                    label={t('fuel_consumption')}
                                />
                            </fieldset>
                        </div>
                    </Tab>
                    <Tab eventKey={2} title={t('view')}>
                        <div className="car-settings-form">
                            <fieldset>
                                <legend>{t('common_settings')}</legend>
                                <Field
                                    name="name"
                                    component={TextField}
                                    type="text"
                                    label={t('name')}
                                    validate={required}
                                />
                                <Field
                                    name="color"
                                    component={ColorPickerField}
                                    label={t('icon_color')}
                                />
                                <Field
                                    name="vehicleGroup"
                                    component={MultipleSelectField}
                                    label={t('group')}
                                    options={groups.map(group => {
                                        return {
                                            name: group.name,
                                            value: group.id
                                        };
                                    })}
                                    format={this.listFormatter}
                                    normalize={this.listNormalizer(groups)}
                                />
                                <Field
                                    name="icon"
                                    component={IconSelectField}
                                    label={t('icon')}
                                    options={iconTypes.map(icon => {
                                        return {
                                            name: icon.type,
                                            value: icon.id
                                        };
                                    })}
                                    icons={iconTypes}
                                    format={this.listFormatter}
                                    normalize={this.listNormalizer(iconTypes)}
                                    color={color}
                                />
                            </fieldset>
                        </div>
                    </Tab>
                </Tabs>
                <FormGroup>
                    <Col sm={10} smOffset={2}>
                        <Button type="submit">{t('forms:save')}</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

CarSettingsForm = reduxForm({
    form: NAME
})(CarSettingsForm);

const selector = formValueSelector(NAME);

CarSettingsForm = connect(state => {
    const color = selector(state, 'color');
    return {
        color
    }
})(CarSettingsForm);

export default translate('car', {wait: true})(CarSettingsForm);