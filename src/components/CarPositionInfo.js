import * as React from "react";
import {InfoWindow} from "react-google-maps";
import moment from "moment";
import 'moment/locale/ru';
import {APP_CONFIG} from "../config/index";
import {translate} from "react-i18next";

const HUMAN_COORDINATE_PRECISION = 1000;

const google = window.google;

class CarPositionInfo extends React.Component {
    constructor(props) {
        super(props);

        moment.lang(props.i18n.language);
    }

    render() {
        const {latitude, longitude, onClose, carName, address, time, t} = this.props;

        const normalizedLatitude = Math.round(latitude * HUMAN_COORDINATE_PRECISION) / HUMAN_COORDINATE_PRECISION;
        const normalizedLongitude = Math.round(longitude * HUMAN_COORDINATE_PRECISION) / HUMAN_COORDINATE_PRECISION;

        return (
            <InfoWindow
                onCloseClick={onClose}
                position={new google.maps.LatLng(latitude, longitude)}
            >
                <div>
                    <p>{carName}</p>
                    <p>{address}</p>
                    <p>{`${t('latitude_short')} - ${normalizedLatitude}, ${t('longitude_short')} - ${normalizedLongitude}`}</p>
                    <p>{time}</p>
                </div>
            </InfoWindow>
        );
    }
}

CarPositionInfo.defaultProps = {
    time: moment().format(APP_CONFIG.time.format)
};

export default translate('monitoring', {wait: true})(CarPositionInfo);
