import React from 'react'
import {Alert} from 'react-bootstrap';

import '../components/Home.css';
import {translate} from "react-i18next";

class EventItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {event: {vehicleName, type, value, time, geozoneName}, t} = this.props;

        let message = "",
            style = "info";

        switch (type) {
            case "geozone_location":
                message = value === "in" ? t('zone_location_message_in', {
                    vehicleName,
                    geozoneName,
                    time
                }) : t('zone_location_message_out', {
                    vehicleName,
                    geozoneName,
                    time
                });
                style = value === "in" ? "info" : "warning";

                break;
            case "geozone_speed":
                message = t('zone_speed_message', {
                    vehicleName,
                    value,
                    geozoneName,
                    time
                });
                style = "danger";

                break;
            default:
                message = t('unsupported_event');
        }

        return (
            <Alert bsStyle={style}>
                {message}
            </Alert>
        )
    }
}

export default translate('events', {wait: true})(EventItem);
