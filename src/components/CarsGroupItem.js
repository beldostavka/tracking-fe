import * as React from "react";
import {ListGroup, Panel} from "react-bootstrap";
import CarListItem from "./CarListItem";

export default class CarsGroupItem extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {group, cars, onUpdateCar, zoomCar, clearZoom, zoomCarId, onEditCar} = this.props;

        let carId;

        const carsItems = cars.map(car => {
            carId = car.id;

            return <CarListItem
                key={carId}
                eventKey={carId}
                carName={car.name}
                carId={carId}
                onMonitorChange={onUpdateCar}
                onEditCar={onEditCar}
                zoomCar={zoomCar}
                clearZoom={clearZoom}
                isZoomed={zoomCarId === carId}
                monitor={car.monitor}
            />
        });

        return <Panel
            collapsible
            defaultExpanded
            header={`${group.name} (${carsItems.length})`}
            key={group.id}
        >
            <ListGroup fill>
                {carsItems}
            </ListGroup>
        </Panel>
    }
}
