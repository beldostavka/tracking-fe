import React from 'react';
import {Accordion, Panel} from "react-bootstrap";
import ZoneForm from "./ZoneForm";

export default class ZonesEditableList extends React.Component {
    static FORM_NAME = "zoneSettings";

    constructor(props) {
        super(props);
    }

    render() {
        const {onUpdate, onDelete, onZoneClick, zones} = this.props;

        return (
            <Accordion>
                {zones.map(zone => {
                    const {id, name} = zone;

                    return <Panel
                        header={name}
                        eventKey={id}
                        key={id}
                        onClick={() => {
                            onZoneClick(id)
                        }}
                    >
                        <ZoneForm
                            form={`${ZonesEditableList.FORM_NAME}-${id}`}
                            initialValues={zone}
                            onUpdate={onUpdate}
                            onDelete={onDelete}
                        />
                    </Panel>
                })}
            </Accordion>
        );
    }
}
