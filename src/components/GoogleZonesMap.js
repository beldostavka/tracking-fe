import React from 'react';
import {withGoogleMap, GoogleMap} from "react-google-maps";
import DrawingManager from "react-google-maps/lib/components/drawing/DrawingManager";
import Zone from "./Zone";
import {APP_CONFIG} from "../config/index";

const google = window.google;

const WrappedMap = withGoogleMap(props => (
    <GoogleMap
        ref={props.onMapLoad}
        defaultZoom={APP_CONFIG.monitoring.defaultZoom}
        defaultCenter={{ lat: 53.901311, lng: 27.558996 }}
    >
        {props.children}
    </GoogleMap>
));

export default class GoogleZonesMap extends React.Component {
    mapInstance;

    constructor(props) {
        super(props);
    }

    onMapLoad = map => {
        this.mapInstance = map;
    };

    componentWillReceiveProps(nextProps) {
        const {zoneToZoom} = nextProps;
        const {zones} = this.props;

        const zone = zoneToZoom && zones && zones.find(zone => (zone.id === zoneToZoom)) || null;

        if (!zone) {
            return;
        }

        const bounds = zone.bounds || [];

        let googleBounds = new google.maps.LatLngBounds();

        bounds.forEach(bound => {
            googleBounds.extend(new google.maps.LatLng(bound.lat, bound.lng));
        });

        this.mapInstance.fitBounds(googleBounds);
    }

    onPolygonComplete = polygon => {
        const {onZoneComplete} = this.props;

        let paths = polygon.getPaths().pop(),
            bounds = [];

        if (paths) {
            bounds = paths.getArray().map(path => {
                return {
                    lat: path.lat(),
                    lng: path.lng()
                };
            });
        } else {
            console.error('Unable to get a paths from polygon bounds');
        }

        onZoneComplete(bounds);

        return false;
    };

    render() {
        const {zones} = this.props;

        return (
            <WrappedMap
                containerElement={
                    <div style={{ height: `100%` }} />
                }
                mapElement={
                    <div style={{ height: `100%` }} />
                }
                onMapLoad={this.onMapLoad}
            >
                <DrawingManager
                    defaultDrawingMode={google.maps.drawing.OverlayType.POLYGON}
                    defaultOptions={{
                        drawingControl: true,
                        drawingControlOptions: {
                            position: google.maps.ControlPosition.TOP_CENTER,
                            drawingModes: [
                                google.maps.drawing.OverlayType.POLYGON
                            ],
                        },
                        polygonOptions: {
                            clickable: true,
                            draggable: true,
                            editable: true,
                            visible: true,
                            zIndex: 999
                        }
                    }}
                    onPolygonComplete={this.onPolygonComplete}
                />
                {zones && zones.map(zone => {
                    return <Zone
                        zone={zone}
                        key={zone.id}
                    />;
                })}
            </WrappedMap>
        );
    }
}
