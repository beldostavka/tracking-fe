import * as React from "react";
import {Button, Col, Glyphicon, ListGroupItem, Row} from "react-bootstrap";
import "../styles/CarListItem.css";
import {translate} from "react-i18next";

class CarListItem extends React.Component {
    constructor(props) {
        super(props);
    }

    onClick = () => {
        if (this.props.isZoomed) {
            this.props.clearZoom();
        } else {
            this.props.zoomCar(this.props.carId);
        }
    };

    onMonitorChange = event => {
        event.preventDefault();

        this.props.onMonitorChange(this.props.carId, {
            monitor: !this.props.monitor
        });
    };

    onEditClick = event => {
        event.preventDefault();

        this.props.onEditCar(this.props.carId);
    };

    render() {
        let monitorControl;

        const {monitor, isZoomed, carName, t} = this.props;

        if (monitor) {
            monitorControl = <Glyphicon glyph="eye-open" />;
        } else {
            monitorControl = <Glyphicon glyph="eye-close" />;
        }

        return (
            <ListGroupItem>
                <Row>
                    <Col sm={9} md={9}>
                        <Button
                            onClick={this.onClick}
                            bsStyle="link"
                            active={isZoomed}
                            block
                        >
                            {carName}
                            {isZoomed && ` (${t('monitoring')})`}
                        </Button>
                    </Col>
                    <Col sm={1} md={1} className="monitor-car-toggle">
                        <a onClick={this.onMonitorChange} href="#">
                            {monitorControl}
                        </a>
                    </Col>
                    <Col sm={1} md={1} className="monitor-car-toggle">
                        <a onClick={this.onEditClick} href="#">
                            <Glyphicon glyph="pencil" />
                        </a>
                    </Col>
                </Row>
            </ListGroupItem>
        );
    }
}

export default translate('monitoring', {wait: true})(CarListItem);
