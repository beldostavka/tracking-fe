import * as React from "react";
import {Alert, Button, Form, FormGroup} from "react-bootstrap";
import {APP_CONFIG} from "../config/index";
import {Field, reduxForm} from "redux-form";
import {DateTimeRangePickerField, SelectField} from "./FormFields";
import {required} from "../utils/validators";
import {translate} from "react-i18next";

class CarsTripsForm extends React.Component {
    static NAME = "carTrips";

    constructor(props) {
        super(props);
    }

    onSubmit = data => {
        this.props.onSubmit(
            data.car,
            data.time.start,
            data.time.end,
            CarsTripsForm.NAME
        );
    };

    render() {
        const {cars, handleSubmit, error, t} = this.props;

        let options = cars.map(car => {
            return {
                name: car.name,
                value: car.id
            };
        });

        options.unshift({
            name: t('select_car'),
            value: "",
            disabled: true
        });

        return (
            <Form onSubmit={handleSubmit(this.onSubmit)}>
                {error &&
                <Alert bsStyle="danger">
                    <strong>{t('forms:error_occurred')}!</strong> {error}
                </Alert>
                }
                <Field
                    name="car"
                    component={SelectField}
                    validate={required}
                    options={options}
                    label={t('select_object')}
                    inline={false}
                />
                <Field
                    name="time"
                    component={DateTimeRangePickerField}
                    label={t('select_date_range')}
                    dateFormat={APP_CONFIG.time.formatFilter}
                    dateLimit={APP_CONFIG.trip.filter.maxRangeLimit}
                />
                <FormGroup>
                    <Button type="submit">{t('build_trips')}</Button>
                </FormGroup>
            </Form>
        )
    }
}

CarsTripsForm = reduxForm({
    form: CarsTripsForm.NAME
})(CarsTripsForm);

export default translate('trips', {wait: true})(CarsTripsForm);
