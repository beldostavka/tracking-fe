import React, {Component} from "react";
import {reduxForm, Field} from "redux-form";
import {SelectField, DateTimeRangePickerField} from "./FormFields";
import {required} from "../utils/validators";
import {Button, Col, Form, FormGroup, Row, Alert, ButtonToolbar} from "react-bootstrap";
import {APP_CONFIG} from "../config/index";

class CarsReportForm extends Component {
    static NAME = "reportFormObject";

    constructor(props) {
        super(props);
    }

    onSubmit = data => {
        this.props.onSubmit({
            car: data.car,
            from: data.time.start,
            to: data.time.end,
            report: data.report,
            showOnMap: data.showOnMap

        })
        ;
    };

    render() {
        //console.log(this.props.initialValues);
        let optionsCar = this.props.cars.map(car => {
            return {
                name: car.name,
                value: car.id
            };
        });

        let optionReport = this.props.reportTypes.map(report => {
            return {
                name: report.name,
                value: report.value
            };
        });
        return (<Form onSubmit={this.props.handleSubmit(this.onSubmit)}>

                {this.props.error &&
                <Alert bsStyle="danger">
                    <strong>Произошла ошибка!</strong> {this.props.error}
                </Alert>
                }

                <div className="reports-form">

                    <legend>Обьект</legend>
                    <Row>
                        <Col xs={4} md={8}>
                            <Field
                                name="car"
                                component={SelectField}
                                validate={required}
                                inline={false}
                                options={optionsCar}
                                label="Выберите объект"
                            />
                        </Col>
                        <Col xs={2} md={4}>
                            <label htmlFor="employed">Показать на карте</label>
                            <Field
                                name="showOnMap"
                                id="showOnMap"
                                component="input"
                                type="checkbox"
                                disabled="true"
                            />
                        </Col>
                    </Row>
                    <Field
                        name="report"
                        component={SelectField}
                        validate={required}
                        inline={false}
                        options={optionReport}
                        label="Выберите отчет"

                    />

                    <hr/>

                    <h4>Выберите промежуток времени</h4>

                    <Field
                        name="time"
                        component={DateTimeRangePickerField}
                        label="Выберите промежуток времени"
                        dateFormat={APP_CONFIG.time.formatFilter}
                        dateLimit={APP_CONFIG.trip.filter.maxRangeLimit}
                    />

                    <hr/>

                    <FormGroup>
                        <Row>
                            <ButtonToolbar>
                                <Col xs={4} md={6}>
                                    <Button type="submit" bsStyle="primary" bsSize="default"
                                    >
                                        Построить отчет
                                    </Button>
                                </Col>
                                <Col xs={4} md={6}>
                                    <Button bsStyle="primary" bsSize="default"
                                            onClick={this.props.clearReport}>
                                        Очистить отчет
                                    </Button>
                                </Col>
                            </ButtonToolbar>
                        </Row>
                    </FormGroup>

                    <hr/>

                </div>
            </Form>
        )
    }
}
CarsReportForm = reduxForm({
    form: CarsReportForm.NAME
})(CarsReportForm);

export default CarsReportForm;