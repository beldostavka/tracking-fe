import * as React from "react";
import {Polyline} from "react-google-maps";

const COORDINATE_MATCH_PRECISION = 0.01;

const google = window.google;

export default class CarTrip extends React.Component {
    constructor(props) {
        super(props);
    }

    onTripClick = event => {
        const lat = event.latLng && event.latLng.lat() || null,
            lng = event.latLng && event.latLng.lng() || null;

        const matchedCoordinate = this.matchCoordinateByLatLng(lat, lng);

        let coordinate = {
            latitude: lat,
            longitude: lng
        };

        coordinate.time = matchedCoordinate && matchedCoordinate.source && matchedCoordinate.source.coordDate || Date.now()

        this.props.onTripClick(coordinate, this.props.carId);
    };

    matchCoordinateByLatLng = (latitude, longitude) => {
        if (!Number.isFinite(latitude) || !Number.isFinite(longitude)) {
            return null;
        }

        const coordinates = this.props.coordinates || [];

        let location,
            index,
            length = coordinates.length;

        for (index = 0; index < length; index++) {
            location = coordinates[index].location;

            if (Math.abs(latitude - location.latitude) <= COORDINATE_MATCH_PRECISION
                && Math.abs(longitude - location.longitude) <= COORDINATE_MATCH_PRECISION) {
                return coordinates[index];
            }
        }

        return null;
    };

    render() {
        const settings = this.props.settings;

        return (
            <Polyline
                path={this.props.coordinates.map(coordinate => {
                    const location = coordinate.location || {};

                    return {
                        lat: location.latitude,
                        lng: location.longitude
                    };
                })}
                onClick={this.onTripClick}
                options={{
                    icons: [
                        {
                            icon: {
                                path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
                            },
                            offset: "100%"
                        },
                        {
                            icon: {
                                path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                            },
                            offset: "0"
                        }
                    ],
                    strokeColor: this.props.color,
                    strokeWeight: settings.strokeWeight
                }}
            >
            </Polyline>
        );
    }
}
