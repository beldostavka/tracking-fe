import React from 'react';
import {Alert, Button, ButtonToolbar, Col, Form, FormGroup} from "react-bootstrap";
import "../styles/CarSettings.css";
import {Field, reduxForm} from "redux-form";
import {SelectField} from "./FormFields";
import {required} from "../utils/validators";
import {translate} from "react-i18next";

class CarEventForm extends React.Component {
    constructor(props) {
        super(props);
    }

    onSubmit = data => {
        this.props.onCreate(data);
    };

    render() {
        const {error, handleSubmit, submitSucceeded, cars, zones, events, t} = this.props;

        return (
            <Form horizontal onSubmit={handleSubmit(this.onSubmit)}>
                {error &&
                    <Alert bsStyle="danger">
                        <strong>{t('error_occurred')}!</strong> {error}
                    </Alert>
                }
                {submitSucceeded &&
                    <Alert bsStyle="success">{t('save_success')}</Alert>
                }
                <Field
                    name="vehicle"
                    component={SelectField}
                    options={cars.map(car => {
                        return {
                            name: car.name,
                            value: car.id
                        };
                    })}
                    validator={required}
                    label={t('labels:car')}
                />
                <Field
                    name="geozone"
                    component={SelectField}
                    options={zones.map(zone => {
                        return {
                            name: zone.name,
                            value: zone.id
                        };
                    })}
                    validator={required}
                    label={t('labels:zone')}
                />
                <Field
                    name="eventType"
                    component={SelectField}
                    options={events.map(event => {
                        return {
                            name: event.type,
                            value: event.id
                        };
                    })}
                    validator={required}
                    label={t('labels:event')}
                />
                <FormGroup>
                    <Col sm={10} smOffset={2}>
                        <ButtonToolbar>
                            <Button type="submit">{t('save')}</Button>
                        </ButtonToolbar>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

CarEventForm = translate('forms', {wait: true})(CarEventForm);

export default reduxForm({})(CarEventForm);
