import * as React from "react";
import {Marker} from "react-google-maps";
import CarPositionInfo from "./CarPositionInfo";
import moment from "moment";
import {APP_CONFIG} from "../config/index";

const google = window.google;

export default class CarMarker extends React.Component {
    constructor(props) {
        super(props);
    }

    onClick = () => {
        const {coordinate: {lattitude, longitude, coordDate}, car: {id}, onCarClick} = this.props;

        onCarClick(
            {
                latitude: lattitude,
                time: coordDate,
                longitude
            },
            id
        );
    };

    render() {
        const {car: {id, name, color, icon}, geocode, coordinate: {lattitude, longitude, bearing}, zoom, onGeoCodeClose} = this.props;

        return (
            <div>
                <Marker
                    position={{ lat: lattitude, lng: longitude }}
                    icon={ {
                        path: icon && icon.path || null,
                        // TODO: make it more clever
                        anchor: bearing >= 180 ? null : new google.maps.Point(35,55),
                        rotation: icon.type !== 'Человек' ? bearing : 0,
                        fillColor: color,
                        strokeColor: color,
                        fillOpacity: 1,
                        strokeOpacity: 1,
                        scale: APP_CONFIG.monitoring.scaleZoomRatio[zoom]
                    }}
                    onClick={this.onClick}
                />
                {geocode &&
                    <CarPositionInfo
                        onClose={() => onGeoCodeClose(id)}
                        key={id}
                        carName={name}
                        latitude={geocode.coordinate.latitude}
                        longitude={geocode.coordinate.longitude}
                        address={geocode.address}
                        time={moment(geocode.coordinate.time).format(APP_CONFIG.time.format)}
                    />
                }
            </div>
        );
    }
}
