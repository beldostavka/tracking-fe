import React from 'react';
import {withGoogleMap, GoogleMap} from "react-google-maps";
import CarTrips from "./CarTrips";
import TripsPlayHeads from "./TripsPlayHeads";
import {APP_CONFIG} from "../config/index";

const UNDEFINED_CAR = "Неопознанная машина";

const WrappedMap = withGoogleMap(props => (
    <GoogleMap
        defaultZoom={APP_CONFIG.monitoring.defaultZoom}
        defaultCenter={{ lat: 53.901311, lng: 27.558996 }}
    >
        {props.trips}
        {props.tripsPlayHeads}
    </GoogleMap>
));

export default class GoogleTripsMap extends React.Component {
    constructor(props) {
        super(props);
    }

    buildTrips = (carsTrips, tracksGeocode, cars) => {
        let carId,
            car,
            trips = [],
            carTrips;

        for (carId in carsTrips) {
            if (!carsTrips.hasOwnProperty(carId)) {
                continue;
            }

            carTrips = carsTrips[carId];

            car = cars.find(car => car.id === carId) || { name: UNDEFINED_CAR };

            trips.push(<CarTrips
                tripsCoordinates={carTrips.coordinates}
                settings={carTrips.settings}
                onTripClick={this.props.onTripClick}
                car={car}
                key={carId}
                trackGeocode={tracksGeocode[carId]}
                onGeoCodeClose={this.props.onGeoCodeClose}
            />);
        }

        return trips;
    };

    render() {
        return (
            <WrappedMap
                containerElement={
                    <div style={{ height: `100%` }} />
                }
                mapElement={
                    <div style={{ height: `100%` }} />
                }
                trips={this.buildTrips(
                    this.props.trips,
                    this.props.tracksGeocode,
                    this.props.cars
                )}
                tripsPlayHeads={<TripsPlayHeads
                    tripsCoordinate={this.props.tripsPlayHead}
                    onPlayHeadClose={this.props.onPlayHeadClose}
                />}
            />
        );
    }
}
