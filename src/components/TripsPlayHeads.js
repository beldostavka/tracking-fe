import * as React from "react";
import {InfoWindow} from "react-google-maps";
import moment from "moment";
import {APP_CONFIG} from "../config/index";

const google = window.google;

const HUMAN_COORDINATE_PRECISION = 1000;

export default class TripsPlayHeads extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const tripsCoordinate = this.props.tripsCoordinate;

        let carId,
            coordinate,
            latitude,
            longitude,
            playHeads = [];

        for (carId in tripsCoordinate) {
            if (!tripsCoordinate.hasOwnProperty(carId)) {
                continue;
            }

            coordinate = tripsCoordinate[carId];

            latitude = coordinate.location.latitude;
            longitude = coordinate.location.longitude;

            playHeads.push(<InfoWindow
                onCloseClick={() => this.props.onPlayHeadClose(carId)}
                position={new google.maps.LatLng(latitude, longitude)}
                key={carId}
            >
                <div>
                    <p>
                        Ш - {Math.round(latitude * HUMAN_COORDINATE_PRECISION) / HUMAN_COORDINATE_PRECISION}, Д - {Math.round(longitude * HUMAN_COORDINATE_PRECISION) / HUMAN_COORDINATE_PRECISION}
                    </p>
                    <p>{moment(coordinate.source.coordDate).format(APP_CONFIG.time.format)}</p>
                </div>
            </InfoWindow>)
        }

        return (
            <div>
                {playHeads}
            </div>
        );
    }
}
