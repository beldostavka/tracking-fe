import React from 'react'
import EventItem from "./EventItem";
import '../components/Home.css';

export default class EventsList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {events} = this.props;

        return (
            <div>
                {Array.isArray(events) && events.map((event, index) => {
                    return <EventItem
                        event={event}
                        key={index}
                    />
                })}
            </div>
        )
    }
}
