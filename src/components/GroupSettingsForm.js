import React from 'react';
import {Alert, Button, ButtonToolbar, Col, Form, FormGroup, Glyphicon} from "react-bootstrap";
import "../styles/CarSettings.css";
import {Field, reduxForm} from "redux-form";
import {TextField} from "./FormFields";
import {required} from "../utils/validators";
import {translate} from "react-i18next";

class GroupSettingsForm extends React.Component {
    constructor(props) {
        super(props);
    }

    onSubmit = data => {
        const {initialValues} = this.props;

        if (initialValues.id) {
            this.props.onUpdate(initialValues.id, data);
        } else {
            this.props.onCreate(data);
        }
    };

    onDelete = () => {
        this.props.onDelete(this.props.initialValues.id);
    };

    render() {
        const group = this.props.initialValues;
        const {t, error, handleSubmit, submitSucceeded} = this.props;

        return (
            <Form horizontal onSubmit={handleSubmit(this.onSubmit)}>
                {error &&
                <Alert bsStyle="danger">
                    <strong>{t('error_occurred')}!</strong> {error}
                </Alert>
                }
                {submitSucceeded && <Alert bsStyle="success">{t('save_success')}</Alert>}

                <Field
                    name="name"
                    component={TextField}
                    type="text"
                    label={t('labels:name')}
                    validate={required}
                />
                <Field
                    name="contragentId"
                    type="hidden"
                    component="input"
                />
                <FormGroup>
                    <Col sm={10} smOffset={2}>
                        <ButtonToolbar>
                            <Button type="submit">{t('save')}</Button>
                            {group.id &&
                                <Button
                                    bsStyle="danger"
                                    onClick={this.onDelete}
                                ><Glyphicon glyph="trash" /></Button>
                            }
                        </ButtonToolbar>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

GroupSettingsForm = reduxForm({})(GroupSettingsForm);

export default translate('forms', {wait: true})(GroupSettingsForm);
