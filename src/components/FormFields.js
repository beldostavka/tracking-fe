import {Col, ControlLabel, FormControl, FormGroup, HelpBlock, Row} from "react-bootstrap";
import React from 'react';
import {TwitterPicker} from "react-color";
import DatetimeRangePicker from "react-bootstrap-datetimerangepicker";

const HorizontalField = props => {
    const {name, error, touched, control, label} = props;

    return <FormGroup
        controlId={name}
        validationState={error && "error" || "success"}
    >
        <Col componentClass={ControlLabel} sm={2}>
            {label}
        </Col>
        <Col sm={10}>
            {control}
            <FormControl.Feedback/>
            {touched && error && <HelpBlock>{error}</HelpBlock>}
        </Col>
    </FormGroup>
};

const VerticalField = props => {
    const {name, error, touched, control, label} = props;

    return <FormGroup controlId={name}  validationState={error && "error" || "success"}>
        <ControlLabel>{label}</ControlLabel>
        {control}
        <FormControl.Feedback/>
        {touched && error && <HelpBlock>{error}</HelpBlock>}
    </FormGroup>
};

const Field = props => {
    const {control, label, touched, error, name, inline, disabled} = props;

    if (inline) {
        return <HorizontalField
            control={control}
            label={label}
            touched={touched}
            error={error}
            name={name}
        />;
    } else {
        return <VerticalField
            control={control}
            label={label}
            touched={touched}
            error={error}
            name={name}
        />
    }
};

export const TextField = ({ input, label, type, meta: { touched, error }, inline = true, disabled = false }) => {
    const control = <FormControl
        disabled={disabled}
        {...input}
        type={type}
        placeholder={label}
    />;

    return <Field
        control={control}
        label={label}
        touched={touched}
        error={error}
        name={input.name}
        inline={inline}
    />;
};

export const SelectField = ({ input, label, type, meta: { touched, error }, options, inline = true }) => {
    if (!(Array.isArray(options) && options.length > 0)) {
        return null;
    }

    const control = <FormControl
        componentClass="select"
        {...input}
        type={type}
        placeholder={label}
    >
        {options.map((option, index) => {
            return <option key={index} value={option.value} disabled={option.disabled}>{option.name}</option>
        })}
    </FormControl>;

    return <Field
        control={control}
        label={label}
        touched={touched}
        error={error}
        name={input.name}
        inline={inline}
    />;
};

export const IconSelectField = props => {
    const {input: {value}, icons, color} = props;

    const selectedIcon = Array.isArray(icons) && icons.find(icon => icon.id === value) || null;

    return <div>
        <SelectField {...props}  />
        <Row>
            <Col sm={2} />
            <Col sm={10}>
                {selectedIcon && <svg xmlns="http://www.w3.org/2000/svg" width={30} viewBox="0 0 35 55"><g><path fill={color} d={selectedIcon.path}/></g></svg>}
            </Col>
        </Row>
    </div>;
};

export const MultipleSelectField = ({ input, label, meta: { touched, error }, options, inline = true }) => {
    if (!(Array.isArray(options) && options.length > 0)) {
        return null;
    }

    const control = <FormControl
        componentClass="select"
        {...input}
        multiple
    >
        {options.map((option, index) => {
            return <option key={index} value={option.value} disabled={option.disabled}>{option.name}</option>
        })}
    </FormControl>;

    return <Field
        control={control}
        label={label}
        touched={touched}
        error={error}
        name={input.name}
        inline={inline}
    />;
};

export class ColorPickerField extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            displayPicker: false
        };
    }

    onDisplayColorPicker = () => {
        this.setState({
            displayPicker: !this.state.displayPicker
        });
    };

    render() {
        const { input: {value, onChange}, label, name } = this.props;

        return (
            <FormGroup
                controlId={name}
            >
                <Col componentClass={ControlLabel} sm={2}>
                    {label}
                </Col>
                <Col sm={10}>
                    <div className="car-settings-color" onClick={this.onDisplayColorPicker} style={{backgroundColor: value}}>{value}</div>
                    {this.state.displayPicker &&
                        <TwitterPicker onChangeComplete={color => { onChange(color.hex)} } />
                    }
                </Col>
            </FormGroup>
        )
    }
}

export class DateTimeRangePickerField extends React.Component {
    render() {
        const { dateFormat, dateLimit, input: {value, onChange}, label, name } = this.props;

        const start = value.start.format(dateFormat);
        const end = value.end.format(dateFormat);

        let timeRange = start + ' - ' + end;

        if (start === end) {
            timeRange = start;
        }

        return (
            <FormGroup controlId={name}>
                <ControlLabel>{label}</ControlLabel>
                <DatetimeRangePicker
                    timePicker
                    timePicker24Hour
                    showDropdowns
                    timePickerSeconds
                    start={value.start}
                    end={value.end}
                    onApply={(event, picker) => { onChange({
                        start: picker.startDate,
                        end: picker.endDate
                    })} }
                    dateLimit={dateLimit}
                >
                    <FormControl type="text" value={timeRange} readOnly />
                </DatetimeRangePicker>
            </FormGroup>
        )
    }
}
