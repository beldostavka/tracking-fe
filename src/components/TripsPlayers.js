import * as React from "react";
import TripsPlayer from "./TripsPlayer";

export default class TripsPlayers extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let carId,
            carTrips,
            carTripsCoordinates;

        const carsTrips = this.props.trips;

        const players = this.props.cars.map((car) => {
            carId = car.id;

            if (!carId) {
                return null;
            }

            carTrips = carsTrips[carId];

            carTripsCoordinates = carTrips && carTrips.coordinates || [];

            if (!(Array.isArray(carTripsCoordinates) && carTripsCoordinates.length > 0)) {
                return null;
            }

            return <TripsPlayer
                car={car}
                coordinates={[].concat.apply([], carTripsCoordinates)}
                key={carId}
                onPlayheadUpdate={this.props.updateCarPlayHead}
                onClose={this.props.clearCarTrips}
                settings={carTrips.settings}
            />;
        });

        return (
            <div>
                {players}
            </div>
        );
    }
}
