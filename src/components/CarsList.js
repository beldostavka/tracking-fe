import React from 'react';
import CarListItem from "./CarListItem";
import {Button, Col, Glyphicon, Panel, PanelGroup, Row} from "react-bootstrap";
import CarsGroupItem from "./CarsGroupItem";
import {translate} from "react-i18next";

const CarsList = class extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            toEditCarId: null
        };
    }

    monitorAll = () => {
        const {onUpdateCars, cars} = this.props;

        onUpdateCars(cars.map(car => {
            return {
                id: car.id,
                monitor: true
            };
        }));
    };

    stopMonitorAll = () => {
        const {onUpdateCars, cars} = this.props;

        onUpdateCars(cars.map(car => {
            return {
                id: car.id,
                monitor: false
            };
        }));
    };

    render() {
        let groupCars,
            carId;

        const {cars, groups, onUpdateCar, zoomCar, clearZoom, zoomCarId, onEditCar, t} = this.props;

        const groupedItemsList = groups.map(group => {
            groupCars = cars.filter(car => Array.isArray(car.vehicleGroup) && car.vehicleGroup.find(carGroup => carGroup.id === group.id));

            return <CarsGroupItem
                group={group}
                cars={groupCars}
                key={group.id}
                onUpdateCar={onUpdateCar}
                zoomCar={zoomCar}
                clearZoom={clearZoom}
                zoomCarId={zoomCarId}
                onEditCar={onEditCar}
            />;
        });

        const unGroupedItemsList = cars
            .filter(car => !(Array.isArray(car.vehicleGroup) && car.vehicleGroup.length > 0))
            .map(car => {
                carId = car.id;

                return <CarListItem
                    key={carId}
                    eventKey={carId}
                    carName={car.name}
                    carId={carId}
                    onMonitorChange={onUpdateCar}
                    onEditCar={onEditCar}
                    zoomCar={zoomCar}
                    clearZoom={clearZoom}
                    isZoomed={zoomCarId === carId}
                    monitor={car.monitor}
                />
            });

        let monitorControl;

        if (cars.find(car => car.monitor)) {
            monitorControl = <Button
                bsStyle="success"
                bsSize="small"
                block
                onClick={this.stopMonitorAll}
            >
                {t('hide_all')} <Glyphicon glyph="eye-open" />
            </Button>;
        } else {
            monitorControl = <Button
                bsStyle="danger"
                bsSize="small"
                block
                onClick={this.monitorAll}
            >
                {t('show_all')} <Glyphicon glyph="eye-close" />
            </Button>;
        }

        return (
            <div>
                <Row className="monitor-car-item">
                    <Col sm={12} md={12} className="monitor-car-toggle">
                        {monitorControl}
                    </Col>
                </Row>
                <Panel header={t('objects_in_groups')} bsStyle="primary">
                    <PanelGroup>
                        {groupedItemsList}
                    </PanelGroup>
                </Panel>
                <Panel header={t('objects_not_in_groups')} bsStyle="success">
                    {unGroupedItemsList}
                </Panel>

            </div>
        );
    }
};

export default translate('monitoring', {wait: true})(CarsList);
