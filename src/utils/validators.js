import i18n from '../i18n';

export const required = value => {return value ? undefined : i18n.t('forms:required')};
