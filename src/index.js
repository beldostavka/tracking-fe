import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import registerServiceWorker from "./registerServiceWorker";
import "./index.css";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap-theme.css";
import {createStore} from "redux";
import {loadCars} from "./actions/cars";
import {Provider} from "react-redux";
import {reducers} from "./reducers/index";
import {middlewares} from "./middleware/index";
import * as moment from "moment";
import 'moment/locale/ru';
import {loadProfile} from "./actions/profile";
import {loadGroups} from "./actions/groups";
import {loadLists} from "./actions/lists";
import {loadZones} from "./actions/zones";
import {loadCarsEvents} from "./actions/carsEvents";
import {loadEvents} from "./actions/events";
import i18n from './i18n';

moment.lang(i18n.language);

i18n.on('languageChanged', function(lng) {
    moment.locale(lng);
});

const store = createStore(
    reducers,
    {},
    middlewares
);

store.dispatch(loadCars());
store.dispatch(loadGroups());
store.dispatch(loadZones());
store.dispatch(loadLists());
store.dispatch(loadCarsEvents());
store.dispatch(loadProfile());
store.dispatch(loadEvents());

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();
