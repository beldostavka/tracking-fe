import React from 'react'
import {Col, Panel, Row} from 'react-bootstrap';

import '../components/Home.css';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {reset} from "redux-form";
import GoogleZonesMap from "../components/GoogleZonesMap";
import ZoneForm from "../components/ZoneForm";
import {setGeoZone} from "../actions/form";
import ZonesEditableList from "../components/ZonesEditableList";
import {createZone, deleteZone, updateZone} from "../actions/zones";
import {translate} from "react-i18next";

const ZONE_SETTINGS_FORM_NAME = 'geoZone';

class ZonesContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            zoneToZoom: null
        };
    }

    onZoneClick = (id) => {
        this.setState({
            zoneToZoom: id
        });
    };

    render() {
        const {onZoneComplete, onUpdateZone, onDeleteZone, onCreateZone, zones, t} = this.props;

        return (
            <Row>
                <Col sm={4}>
                    <Panel header={t('zones')} bsStyle="warning">
                        <ZonesEditableList
                            onUpdate={onUpdateZone}
                            onDelete={onDeleteZone}
                            onZoneClick={this.onZoneClick}
                            zones={zones}
                        />
                    </Panel>
                    <Panel header={t('new_zone')} bsStyle="warning">
                        <ZoneForm
                            form={ZONE_SETTINGS_FORM_NAME}
                            onCreate={onCreateZone}
                            onSubmitSuccess={(result, dispatch) => {
                                dispatch(reset(ZONE_SETTINGS_FORM_NAME))
                            }}
                        />
                    </Panel>
                </Col>
                <Col sm={8} className="map-container">
                    <GoogleZonesMap
                        onZoneComplete={onZoneComplete}
                        zones={zones}
                        zoneToZoom={this.state.zoneToZoom}
                    />
                </Col>
            </Row>
        )
    }
}

function mapStateToProps(state) {
    return {
        zones: state.zones || [],
    };
}

function mapDispatchToProps(dispatch) {
    return {
        onZoneComplete: bindActionCreators(setGeoZone, dispatch),

        onUpdateZone: bindActionCreators(updateZone, dispatch),
        onDeleteZone: bindActionCreators(deleteZone, dispatch),
        onCreateZone: bindActionCreators(createZone, dispatch),
    };
}

ZonesContainer = connect(mapStateToProps, mapDispatchToProps)(ZonesContainer);

export default translate('zones', {wait: true})(ZonesContainer);