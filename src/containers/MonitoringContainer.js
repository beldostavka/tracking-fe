import React from 'react'
import CarsList from '../components/CarsList'
import {Col, Row} from 'react-bootstrap';
import '../components/Home.css'
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {loadLatestCarsCoordinates, updateCar, updateCars} from "../actions/cars";
import {APP_CONFIG} from "../config/index";
import GoogleMonitoringMap from "../components/GoogleMonitoringMap";
import {clearCarGeoCode, clearGeoCode, loadAddressByCoordinate} from "../actions/geocode";
import {clearZoom, setZoomCar} from "../actions/monitoring";
import CarSettingsModal from "../components/CarSettingsModal";

class MonitoringContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            toEditCarId: null
        };
    }

    onEditCar = carId => {
        this.setState({
            toEditCarId: carId
        });
    };

    onCloseEditCar = () => {
        this.setState({
            toEditCarId: null
        });
    };

    componentDidMount() {
        this.props.loadCoordinates();

        this.timer = setInterval(
            () => this.props.loadCoordinates(),
            APP_CONFIG.monitoring.updateFrequency
        );

        this.props.clearGeoCode();
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    filterCoordinatesForMonitorCars = (cars = [], coordinates = {}) => {
        let carId;
        let filteredCoordinates = {};

        cars.forEach(car => {
            carId = car.id;

            if (car.monitor && coordinates.hasOwnProperty(carId)) {
                filteredCoordinates[carId] = coordinates[carId];
            }
        });

        return filteredCoordinates;
    };

    render() {
        const {cars, groups, zoomCar, clearZoom, zoomCarId,
            onUpdateCar, onUpdateCars, iconTypes, fuelTypes, carTypes, coordinates,
            carsGeocode, loadAddressByCoordinate, clearCarGeoCode
        } = this.props;

        const carToEdit = cars
            .find(car => car.id === this.state.toEditCarId) || null;

        return (
            <Row>
                <Col sm={6} md={3}>
                    <CarsList
                        cars={cars}
                        groups={groups}
                        zoomCar={zoomCar}
                        clearZoom={clearZoom}
                        zoomCarId={zoomCarId}
                        onUpdateCar={onUpdateCar}
                        onUpdateCars={onUpdateCars}
                        onEditCar={this.onEditCar}
                    />
                </Col>
                <Col sm={6} md={9} className="map-container">
                    <GoogleMonitoringMap
                        latestCoordinates={this.filterCoordinatesForMonitorCars(
                            cars,
                            coordinates
                        )}
                        cars={cars}
                        onCarClick={loadAddressByCoordinate}
                        carsGeocode={carsGeocode}
                        onGeoCodeClose={clearCarGeoCode}
                        zoomCarId={zoomCarId}
                        iconTypes={iconTypes}
                    />
                </Col>
                <div className="modal-container">
                    <CarSettingsModal
                        car={carToEdit}
                        groups={groups}
                        onClose={this.onCloseEditCar}
                        onUpdate={onUpdateCar}
                        iconTypes={iconTypes}
                        fuelTypes={fuelTypes}
                        carTypes={carTypes}
                    />
                </div>
        </Row>
        )
    }
}

function mapStateToProps(state) {
    return {
        cars: state.cars || [],
        groups: state.groups || [],
        iconTypes: state.lists  && state.lists.iconTypes || [],
        fuelTypes: state.lists  && state.lists.fuelTypes || [],
        carTypes: state.lists  && state.lists.carTypes || [],
        coordinates: state.monitoring && state.monitoring.coordinates || {},
        zoomCarId: state.monitoring && state.monitoring.zoom || null,
        carsGeocode: state.geocode || {}
    }
}

function mapDispatchToProps(dispatch) {
    return {
        loadCoordinates: bindActionCreators(loadLatestCarsCoordinates, dispatch),
        loadAddressByCoordinate: bindActionCreators(loadAddressByCoordinate, dispatch),
        clearCarGeoCode: bindActionCreators(clearCarGeoCode, dispatch),
        clearGeoCode: bindActionCreators(clearGeoCode, dispatch),
        zoomCar: bindActionCreators(setZoomCar, dispatch),
        clearZoom: bindActionCreators(clearZoom, dispatch),
        onUpdateCar: bindActionCreators(updateCar, dispatch),
        onUpdateCars: bindActionCreators(updateCars, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MonitoringContainer);
