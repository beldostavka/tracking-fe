import React from 'react'
import {Col, Panel, Row} from 'react-bootstrap';

import '../components/Home.css';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {reset} from "redux-form";
import CarEventForm from "../components/CarEventForm";
import CarsEventsEditableList from "../components/CarsEventsEditableList";
import {createCarEvent, deleteCarEvent} from "../actions/carsEvents";
import EventsList from "../components/EventsList";
import {translate} from "react-i18next";

class EventsContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {
            cars, zones, onCreateCarEvent, onDeleteCarEvent,
            carsEvents, eventTypes, events, t
        } = this.props;

        return (
            <Row>
                <Col sm={6}>
                    <Panel header={t('events_subscriptions')} bsStyle="warning">
                        <CarsEventsEditableList
                            onDelete={onDeleteCarEvent}
                            carsEvents={carsEvents}
                            cars={cars}
                            zones={zones}
                            events={eventTypes}
                        />
                    </Panel>
                    <Panel header={t('new_event')} bsStyle="warning">
                        <CarEventForm
                            form={CarsEventsEditableList.FORM_NAME}
                            onCreate={onCreateCarEvent}
                            onSubmitSuccess={(result, dispatch) => {
                                dispatch(reset(CarsEventsEditableList.FORM_NAME))
                            }}
                            cars={cars}
                            zones={zones}
                            events={eventTypes}
                        />
                    </Panel>
                </Col>
                <Col sm={6}>
                    <EventsList
                        events={events}
                    />
                </Col>
            </Row>
        )
    }
}

function mapStateToProps(state) {
    return {
        cars: state.cars || [],
        zones: state.zones || [],
        eventTypes: state.lists && state.lists.eventType || [],

        carsEvents: state.carsEvents || [],
        events: state.events || [],
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onCreateCarEvent: bindActionCreators(createCarEvent, dispatch),
        onDeleteCarEvent: bindActionCreators(deleteCarEvent, dispatch),
    }
}

EventsContainer = translate('events', {wait: true})(EventsContainer);

export default connect(mapStateToProps, mapDispatchToProps)(EventsContainer);
