import React from 'react'
import {Row, Col} from 'react-bootstrap';
import CarsReportForm from '../components/CarsReportForm'
import CarsReportBody from '../components/CarsReportBody'
import moment from 'moment'
import {APP_CONFIG} from "../config/index";
import {connect} from 'react-redux'
import {getReportTrips, getReportGeneric, clearReport} from "../actions/report"
import {bindActionCreators} from "redux";

import '../styles/ReportMain.css'


class ReportContainer extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {
        return (

            <Row>
                <Col sm={6} md={3}>
                    <Row>
                        <Col sm={12} md={12}>
                            <CarsReportForm
                                onSubmit={this.props.getGenericReport}
                                initialValues={{
                                    time: {
                                        start: moment().subtract(APP_CONFIG.report.maxRangeLimit, 'days'),
                                        end: moment()
                                    }
                                }}
                                cars={this.props.cars}
                                reportTypes={this.props.reportTypes}
                                loadTripReport
                                clearReport = {this.props.reportClear}
                            />

                        </Col>
                    </Row>
                </Col>
                <Col sm={6} md={9}>
                    <CarsReportBody
                    reportBody = {this.props.reportTable}
                    />
                </Col>
            </Row>

        )
    }
}
function mapStateToProps(state) {
    return {
        cars: state.cars || [],
        groups: state.groups || [],
        reportTypes: state.lists && state.lists.reportVehicleTypes || [],
        reportTable: state.report.reportBody
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getTripReport: bindActionCreators(getReportTrips, dispatch),
        getGenericReport: bindActionCreators(getReportGeneric, dispatch),
        reportClear:bindActionCreators(clearReport, dispatch)

    }
}


export default connect(mapStateToProps, mapDispatchToProps)(ReportContainer);