import React from 'react'
import {Col, Nav, NavItem, Panel, Row} from 'react-bootstrap';

import '../components/Home.css';
import {connect} from "react-redux";
import {updateCar} from "../actions/cars";
import {bindActionCreators} from "redux";
import CarSettingsForm from "../components/CarSettingsForm";
import {destroy, reset} from "redux-form";
import CarsGroupsEditableList from "../components/CarsGroupsEditableList";
import {createGroup, deleteGroup, updateGroup} from "../actions/groups";
import GroupSettingsForm from "../components/GroupSettingsForm";
import {translate} from "react-i18next";

class CarSettingsContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedCarIndex: 0,
        };
    }

    handleSelectCar = carIndex => {
        this.setState({
            selectedCarIndex: carIndex,
        });

        this.props.onSelectCar('carSettings');
    };

    render() {
        const {
            cars, groups, profile, zones,
            onUpdateGroup, onDeleteGroup, onCreateGroup, onUpdateCar,
            iconTypes, fuelTypes, carTypes, t
        } = this.props;

        return (
            <Row>
                <Col sm={4}>
                    <Panel header={t('objects')} bsStyle="warning">
                        <Nav bsStyle="pills" stacked activeKey={this.state.selectedCarIndex} onSelect={this.handleSelectCar}>
                            {cars.map((car, index) => {
                                return <NavItem eventKey={index} key={index}>
                                    {car.name}
                                </NavItem>
                            })}
                        </Nav>
                    </Panel>
                    <Panel header={t('groups:groups')} bsStyle="warning">
                        <CarsGroupsEditableList
                            onUpdate={onUpdateGroup}
                            onDelete={onDeleteGroup}
                            onCreate={onCreateGroup}
                            groups={groups}
                        />
                    </Panel>
                    {profile &&
                        <Panel header={t('groups:new_group')} bsStyle="warning">
                            <GroupSettingsForm
                                form={CarsGroupsEditableList.FORM_NAME}
                                onCreate={onCreateGroup}
                                initialValues={{
                                    contragentId: profile.contragent && profile.contragent.id || null
                                }}
                                onSubmitSuccess={(result, dispatch) => {
                                    dispatch(reset(CarsGroupsEditableList.FORM_NAME))
                                }}
                            />
                        </Panel>
                    }
                </Col>
                <Col sm={8}>
                    <CarSettingsForm
                        initialValues={cars[this.state.selectedCarIndex] || null}
                        onUpdate={onUpdateCar}
                        groups={groups}
                        zones={zones}
                        iconTypes={iconTypes}
                        fuelTypes={fuelTypes}
                        carTypes={carTypes}
                    />
                </Col>
            </Row>
        )
    }
}

function mapStateToProps(state) {
    return {
        cars: state.cars || [],
        groups: state.groups || [],
        zones: state.zones || [],
        profile: state.profile || null,
        iconTypes: state.lists  && state.lists.iconTypes || [],
        fuelTypes: state.lists  && state.lists.fuelTypes || [],
        carTypes: state.lists  && state.lists.carTypes || [],
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onUpdateCar: bindActionCreators(updateCar, dispatch),

        onUpdateGroup: bindActionCreators(updateGroup, dispatch),
        onDeleteGroup: bindActionCreators(deleteGroup, dispatch),
        onCreateGroup: bindActionCreators(createGroup, dispatch),
        // this action creator is used to destroy a form after selecting a new car
        onSelectCar: bindActionCreators(destroy, dispatch),
        onSelectGroup: bindActionCreators(destroy, dispatch),
    }
}

CarSettingsContainer = connect(mapStateToProps, mapDispatchToProps)(CarSettingsContainer);

export default translate('car', {wait: true})(CarSettingsContainer);