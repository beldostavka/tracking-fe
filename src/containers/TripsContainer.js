import React from 'react'
import {Col, Row} from 'react-bootstrap';
import {APP_CONFIG} from "../config/index";
import '../components/Home.css'
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import CarsTripsForm from "../components/CarsTripsForm";
import {loadTimeRangeCarTripsCoordinates} from "../actions/cars";
import {clearCarGeoCode, clearGeoCode, loadAddressByCoordinate} from "../actions/geocode";
import {clearPlayHead, updateCarTrackPlayHead} from "../actions/tracksPlayer";
import {clearTrips} from "../actions/trips";
import TripsPlayers from "../components/TripsPlayers";
import GoogleTripsMap from "../components/GoogleTripsMap";
import moment from "moment";

class TripsContainer extends React.Component {
    componentDidMount() {
        this.props.clearGeoCode();
    }

    render() {
        return (
            <Row>
                <Col sm={6} md={3}>
                    <Row>
                        <Col sm={12} md={12}>
                            <CarsTripsForm
                                cars={this.props.cars}
                                onSubmit={this.props.loadTimeRangeCarTripsCoordinates}
                                initialValues={{
                                    time: {
                                        start: moment().subtract(APP_CONFIG.trip.filter.maxRangeLimit, 'days'),
                                        end: moment()
                                    }
                                }}
                            />
                        </Col>
                    </Row>
                    <TripsPlayers
                        cars={this.props.cars}
                        trips={this.props.trips}
                        updateCarPlayHead={this.props.updateCarPlayHead}
                        clearCarTrips={this.props.clearCarTrips}
                    />
                </Col>
                <Col sm={6} md={9} className="map-container">
                    <GoogleTripsMap
                        cars={this.props.cars}
                        tracksGeocode={this.props.tracksGeocode}
                        trips={this.props.trips}
                        onGeoCodeClose={this.props.clearCarGeoCode}
                        onPlayHeadClose={this.props.clearPlayHead}
                        onTripClick={this.props.loadAddressByCoordinate}
                        tripsPlayHead={this.props.tripsPlayHead}
                    />
                </Col>
            </Row>
        )
    }
}

function mapStateToProps(state) {
    return {
        cars: state.cars || [],
        trips: state.trips || {},
        tracksGeocode: state.geocode || {},
        tripsPlayHead: state.tracksPlayer || {}
    }
}

function mapDispatchToProps(dispatch) {
    return {
        loadAddressByCoordinate: bindActionCreators(loadAddressByCoordinate, dispatch),
        loadTimeRangeCarTripsCoordinates: bindActionCreators(loadTimeRangeCarTripsCoordinates, dispatch),
        clearCarTrips: bindActionCreators(clearTrips, dispatch),
        clearCarGeoCode: bindActionCreators(clearCarGeoCode, dispatch),
        clearGeoCode: bindActionCreators(clearGeoCode, dispatch),
        clearPlayHead: bindActionCreators(clearPlayHead, dispatch),
        updateCarPlayHead: bindActionCreators(updateCarTrackPlayHead, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TripsContainer);
