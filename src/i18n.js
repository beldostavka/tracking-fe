import i18n from 'i18next';
import Backend from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import { reactI18nextModule } from 'react-i18next';

i18n
    .use(Backend)
    .use(LanguageDetector)
    .use(reactI18nextModule)
    .init({
        fallbackLng: 'ru',

        // have a common namespace used around the full app
        ns: ['nav', 'labels'],
        defaultNS: 'labels',

        debug: true,

        interpolation: {
            escapeValue: false, // not needed for react!!
        },

        react: {
            wait: true
        },

        resources: {
            ru: {
                nav: {
                    monitoring: 'Мониторинг',
                    trips: 'Поездки',
                    reports: 'Отчеты',
                    zones: 'Зоны',
                    events: 'События',
                    object_settings: 'Настройки объектов',
                    exit: 'Выход',
                    hello: 'Привет',
                    site_label: 'Cromvel трекинг'
                },
                monitoring: {
                    show_all: 'Показать все',
                    hide_all: 'Скрыть все',
                    objects_in_groups: 'Объекты в группах',
                    objects_not_in_groups: 'Объекты без групп',
                    monitoring: 'отслеживаем',

                    latitude_short: 'Ш',
                    longitude_short: 'Д',
                },
                forms: {
                    error_occurred: 'Произошла ошибка',
                    save_success: 'Данные успешно сохранены',
                    save: 'Сохранить',
                    required: 'Поле обязательно для заполнения'
                },
                events: {
                    events_subscriptions: 'Подписки на события',
                    new_event: 'Новое событие',
                    unsupported_event: 'Неподдерживаемый тип события, обратитесь к разработчику',

                    zone_speed_message: '{vehicleName} превысил скорость {value}км/ч в зоне "{geozoneName}" {time}',
                    zone_location_message_in: '{vehicleName} въехал в зону "{geozoneName}" {time}',
                    zone_location_message_out: '{vehicleName} выехал из зоны "{geozoneName}" {time}',

                    could_not_create: 'Не удалось создать событие, проверьте данные формы'
                },
                labels: {
                    car: 'Автомобиль',
                    zone: 'Зона',
                    event: 'Событие',
                    actions: 'Действия',
                    close: 'Закрыть',
                    name: 'Имя'
                },
                car: {
                    characteristics: 'Характеристики',
                    common_settings: 'Общие параметры',
                    vehicle_type: 'Тип Т/C',
                    registration_number: 'Регистрационный знак',
                    model: 'Модель',
                    engine_settings: 'Параметры двигателя',
                    engine_volume: 'Объем двигателя, см. куб.',
                    fuel_type: 'Вид топлива',
                    load_settings: 'Параметры груза',
                    load_capacity: 'Грузоподъемность, т.',
                    net_volume: 'Полезный объем',
                    car_settings: 'Параметры автомобиля',
                    mileage: 'Пробег',
                    fuel_consumption: 'Расход топлива',
                    view: 'Вид',
                    name: 'Имя',
                    icon_color: 'Цвет иконки',
                    group: 'Группа автомобиля',
                    icon: 'Иконка автомобиля',

                    objects: 'Объекты'
                },
                trips: {
                    select_car: 'Выберите автомобиль',
                    select_object: 'Выберите объект мониторинга',
                    select_date_range: 'Выберите промежуток времени',
                    build_trips: 'Построить поездки',
                    could_not_get_time: 'Не удалось получить время.',
                    no_trips_for_date: 'Передвижений за выбранную дату не найдено'
                },
                zones: {
                    bounds: 'Границы',
                    speed_limit: 'Лимит скорости',
                    color: 'Цвет',
                    zones: 'Зоны',
                    new_zone: 'Новая зона'
                },
                groups: {
                    groups: 'Группы',
                    new_group: 'Новая группа'
                }
            },
            en: {
                nav: {
                    monitoring: 'Monitoring',
                    trips: 'Trips',
                    reports: 'Reports',
                    zones: 'Zones',
                    events: 'Events',
                    object_settings: 'Objects settings',
                    exit: 'Exit',
                    hello: 'Hello',
                    site_label: 'Cromvel tracking',
                },
                monitoring: {
                    show_all: 'Show all',
                    hide_all: 'Hide all',
                    objects_in_groups: 'Objects in groups',
                    objects_not_in_groups: 'Objects without groups',
                    monitoring: 'watching',

                    latitude_short: 'Lat',
                    longitude_short: 'Lng',
                },
                forms: {
                    error_occurred: 'Error occurred',
                    save_success: 'Data is successfully saved',
                    save: 'Save',
                    required: 'Field is required',
                    could_not_save: 'Couldn\'t save, please check your form'
                },
                events: {
                    events_subscriptions: 'Events subscriptions',
                    new_event: 'New event',
                    unsupported_event: 'Unsupported event, please contact developers',

                    zone_speed_message: '{vehicleName} exceeded a speed {value}km/h in zone "{geozoneName}" {time}',
                    zone_location_message_in: '{vehicleName} entered a zone "{geozoneName}" {time}',
                    zone_location_message_out: '{vehicleName} left a zone "{geozoneName}" {time}',

                    could_not_create: 'Couldn\'t create an event, please check your form',
                    could_not_delete: 'Couldn\'t delete an event',
                },
                labels: {
                    car: 'Vehicle',
                    zone: 'Zone',
                    event: 'Event',
                    actions: 'Actions',
                    close: 'Close',
                    name: 'Name'
                },
                car: {
                    characteristics: 'Characteristics',
                    common_settings: 'Common settings',
                    vehicle_type: 'Vehicle type',
                    registration_number: 'Registration number',
                    model: 'Model',
                    engine_settings: 'Engine settings',
                    engine_volume: 'Engine volume',
                    fuel_type: 'Fuel type',
                    load_settings: 'Load settings',
                    load_capacity: 'Load capacity',
                    net_volume: 'Net volume',
                    car_settings: 'Car settings',
                    mileage: 'Mileage',
                    fuel_consumption: 'Fuel consumption',
                    view: 'View',
                    name: 'Name',
                    icon_color: 'Icon color',
                    group: 'Car group',
                    icon: 'Icon',

                    objects: 'Objects'
                },
                trips: {
                    select_car: 'Select a car',
                    select_object: 'Select monitoring object',
                    select_date_range: 'Select date time range',
                    build_trips: 'Show trips',
                    could_not_get_time: 'Couldn\'t get a time.',
                    no_trips_for_date: 'There are no trips for selected date'
                },
                zones: {
                    bounds: 'Bounds',
                    speed_limit: 'Speed limit',
                    color: 'Color',
                    zones: 'Zones',
                    new_zone: 'New zone'
                },
                groups: {
                    groups: 'Groups',
                    new_group: 'New group'
                }
            }
        }
    });

export default i18n;
