import {CREATED_GROUP, DELETED_GROUP, GROUPS_LOADED, UPDATED_GROUP} from "../actions/groups";

export default function (state = [], action) {
    switch (action.type) {
        case GROUPS_LOADED:
            return action.groups || [];

            break;
        default:
            return state;
    }
}
