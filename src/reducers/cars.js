import {CAR_LOADED, CARS_LOADED} from '../actions/cars';

export default function (state = [], action) {
    switch (action.type) {
        case CARS_LOADED:
            return action.cars;

            break;
        case CAR_LOADED:
            const updatedCar = action.car;

            return state.map(car => {
                if (updatedCar.id === car.id) {
                    return updatedCar;
                } else {
                    return car;
                }
            });
            break;
        default:
            return state;
    }
}
