import {CLEAR_CAR_TRACK_PLAYHEAD, UPDATE_CAR_TRACK_PLAYHEAD} from "../actions/tracksPlayer";
import {CLEAR_TRIPS} from "../actions/trips";

export default function (state = {}, action) {
    switch (action.type) {
        case UPDATE_CAR_TRACK_PLAYHEAD:
            return Object.assign({}, state, {
                [action.carId]: action.coordinate
            });

            break;
        case CLEAR_TRIPS:
        case CLEAR_CAR_TRACK_PLAYHEAD:
            delete state[action.carId];

            return {
                ...state
            };
        default:
            return state;
    }
}
