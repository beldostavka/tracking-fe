import {PROFILE_LOADED} from "../actions/profile";

export default function (state = null, action) {
    switch (action.type) {
        case PROFILE_LOADED:
            return action.profile || null;

            break;
        default:
            return state;
    }
}
