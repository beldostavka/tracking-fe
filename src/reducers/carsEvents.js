import {CARS_EVENTS_LOADED} from "../actions/carsEvents";

export default function (state = [], action) {
    switch (action.type) {
        case CARS_EVENTS_LOADED:
            return action.carsEvents || [];

            break;
        default:
            return state;
    }
}
