import {CLEAR_ZOOM, SET_ZOOM_CAR} from "../actions/monitoring";
import {LATEST_CARS_COORDINATES_LOADED} from "../actions/cars";

export default function (state = {}, action) {
    switch (action.type) {
        case SET_ZOOM_CAR:
            return {
                ...state,
                zoom: action.carId
            };
            break;
        case CLEAR_ZOOM:
            delete state.zoom;

            return {
                ...state
            };
            break;
        case LATEST_CARS_COORDINATES_LOADED:
            return Object.assign({}, state, {
                coordinates: action.coordinates
            });

            break;
        default:
            return state;
    }
}
