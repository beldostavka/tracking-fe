import {LISTS_LOADED} from "../actions/lists";

export default function (state = {}, action) {
    switch (action.type) {
        case LISTS_LOADED:
            return action.lists;

            break;
        default:
            return state;
    }
}
