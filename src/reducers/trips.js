import {CLEAR_TRIPS, TRIPS_BUILT} from "../actions/trips";

export default function (state = {}, action) {
    switch (action.type) {
        case TRIPS_BUILT:
            const tripsCoordinates = action.tripsCoordinates || [],
                carId = action.carId;

            if ([].concat.apply([], tripsCoordinates).length === 0) {
                delete state[carId];

                return {
                    ...state
                };
            } else {
                return Object.assign({}, state, {
                    [carId]: {
                        coordinates: tripsCoordinates,
                        settings: action.settings
                    }
                });
            }

            break;
        case CLEAR_TRIPS:
            delete state[action.carId];

            return {
                ...state
            };
            break;
        default:
            return state;
    }
}
