import {POPULATE_REPORT_STATE, CLEAR_REPORT_STATE} from "../../../../tracking-fe11/tracking-fe/src/actions/report"


export default function (state = [], action) {
    switch (action.type) {
        case POPULATE_REPORT_STATE:
            return Object.assign([], state, action.payload);
            break;

        case CLEAR_REPORT_STATE:
            return [];
            break;

        default:
            return state;
    }

}
