import {POPULATE_REPORT_FORM, ADD_ITEM_TO_REPORT_FORM} from '../../../../tracking-fe11/tracking-fe/src/actions/report';


export default function (state = {}, action) {
    switch (action.type) {
        case POPULATE_REPORT_FORM:
            return Object.assign([], state, action.payload);
            break;
        case ADD_ITEM_TO_REPORT_FORM:
            return {...state, [action.name]: action.payload.id};
            break;
        default:
            return state;
    }

}