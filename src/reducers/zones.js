import {ZONES_LOADED} from "../actions/zones";

export default function (state = [], action) {
    switch (action.type) {
        case ZONES_LOADED:
            return action.zones || [];

            break;
        default:
            return state;
    }
}
