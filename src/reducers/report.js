import reportForm from './reportForm';
import reportBody from './reportBody';

import {combineReducers} from 'redux'

export default combineReducers({reportForm, reportBody})