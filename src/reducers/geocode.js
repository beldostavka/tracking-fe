import {CLEAR_CAR_GEO_CODE, CLEAR_GEO_CODE, LOADED_ADDRESS_BY_COORDINATE} from "../actions/geocode";

export default function (state = {}, action) {
    switch (action.type) {
        case LOADED_ADDRESS_BY_COORDINATE:
            return Object.assign({}, state, {
                [action.carId]: {
                    address: action.address,
                    coordinate: action.coordinate
                }
            });

            break;
        case CLEAR_CAR_GEO_CODE:
            delete state[action.carId];

            return {
                ...state
            };

            break;
        case CLEAR_GEO_CODE:
            return {};

            break;
        default:
            return state;
    }
}
