import {EVENTS_LOADED} from "../actions/events";

export default function (state = [], action) {
    switch (action.type) {
        case EVENTS_LOADED:
            return action.events || [];

            break;
        default:
            return state;
    }
}
