import { combineReducers } from 'redux';
import cars from "./cars";
import geocode from "./geocode";
import monitoring from "./monitoring";
import tracksPlayer from "./tracksPlayer";
import trips from "./trips";
import profile from "./profile";
import { reducer as form } from 'redux-form';
import groups from "./groups";
import lists from "./lists";
import zones from "./zones";
import carsEvents from "./carsEvents";
import events from "./events";
import report from "./report";

export const reducers = combineReducers({
    cars,
    trips,
    geocode,
    monitoring,
    tracksPlayer,
    profile,
    groups,
    form,
    lists,
    zones,
    carsEvents,
    events,
    report
});
