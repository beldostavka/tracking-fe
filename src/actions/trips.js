//actions
export const BUILD_TRIPS = 'BUILD_TRIPS';
export const TRIPS_BUILT = 'TRIPS_BUILT';
export const TRIPS_LOADED = 'TRIPS_LOADED';
export const CLEAR_TRIPS = 'CLEAR_TRIPS';

// action creators
export function buildTrips(carId, tripsCoordinates) {
    return {
        type: BUILD_TRIPS,
        carId,
        tripsCoordinates
    };
}
export function tripsBuilt(carId, tripsCoordinates, settings) {
    return {
        type: TRIPS_BUILT,
        carId,
        tripsCoordinates,
        settings
    }
}
export function tripsLoaded(carId, tripsCoordinates) {
    return {
        type: TRIPS_LOADED,
        carId,
        tripsCoordinates
    }
}
export function clearTrips(carId) {
    return {
        type: CLEAR_TRIPS,
        carId
    }
}
