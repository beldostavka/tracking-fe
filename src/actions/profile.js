//actions
export const LOAD_PROFILE = 'LOAD_PROFILE';
export const PROFILE_LOADED = 'PROFILE_LOADED';

// action creators
export function loadProfile() {
    return {
        type: LOAD_PROFILE
    };
}
export function profileLoaded(profile) {
    return {
        type: PROFILE_LOADED,
        profile
    }
}
