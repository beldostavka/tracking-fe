//actions
export const LOAD_EVENTS = 'LOAD_EVENTS';
export const EVENTS_LOADED = 'EVENTS_LOADED';

// action creators
export function loadEvents() {
    return {
        type: LOAD_EVENTS
    };
}
export function eventsLoaded(events) {
    return {
        type: EVENTS_LOADED,
        events
    }
}
