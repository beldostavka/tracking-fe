//actions
export const LOAD_LISTS = 'LOAD_LISTS';
export const LISTS_LOADED = 'LISTS_LOADED';

// action creators
export function loadLists() {
    return {
        type: LOAD_LISTS
    };
}
export function listsLoaded(lists) {
    return {
        type: LISTS_LOADED,
        lists
    }
}
