//actions
export const LOAD_CARS = 'LOAD_CARS';
export const CARS_LOADED = 'CARS_LOADED';

export const LOAD_CAR = 'LOAD_CAR';
export const CAR_LOADED = 'CAR_LOADED';

export const UPDATE_CAR = 'UPDATE_CAR';
export const CAR_UPDATED = 'CAR_UPDATED';
export const CAR_UPDATE_FAILED = 'CAR_UPDATE_FAILED';

export const UPDATE_CARS = 'UPDATE_CARS';
export const CARS_UPDATED = 'CARS_UPDATED';
export const CARS_UPDATE_FAILED = 'CARS_UPDATE_FAILED';

export const LOAD_LATEST_CARS_COORDINATES = 'LOAD_LATEST_CARS_COORDINATES';
export const LATEST_CARS_COORDINATES_LOADED = 'LATEST_CARS_COORDINATES_LOADED';

export const LOAD_TIME_RANGE_CAR_TRIPS_COORDINATES = 'LOAD_TIME_RANGE_CAR_TRIPS_COORDINATES';
export const TIME_RANGE_CAR_TRIPS_COORDINATES_LOADED = 'TIME_RANGE_CAR_TRIPS_COORDINATES_LOADED';
export const TIME_RANGE_CAR_TRIPS_COORDINATES_LOAD_FAILED = 'TIME_RANGE_CAR_TRIPS_COORDINATES_LOAD_FAILED';

// action creators
export function loadCars() {
    return {type: LOAD_CARS};
}
export function carsLoaded(cars) {
    return {
        type: CARS_LOADED,
        cars: cars
    }
}

export function loadCar(carId) {
    return {
        type: LOAD_CAR,
        carId
    };
}
export function carLoaded(car) {
    return {
        type: CAR_LOADED,
        car
    }
}

export function updateCar(carId, payload) {
    return {
        type: UPDATE_CAR,
        carId,
        payload
    };
}
export function carUpdated(carId) {
    return {
        type: CAR_UPDATED,
        carId
    };
}
export function carUpdateFailed(carId, reason) {
    return {
        type: CAR_UPDATE_FAILED,
        carId,
        reason
    };
}

export function updateCars(payload) {
    return {
        type: UPDATE_CARS,
        payload
    };
}
export function carsUpdated() {
    return {
        type: CARS_UPDATED,
    };
}
export function carsUpdateFailed(reason) {
    return {
        type: CARS_UPDATE_FAILED,
        reason
    };
}

export function loadLatestCarsCoordinates() {
    return {type: LOAD_LATEST_CARS_COORDINATES};
}
export function latestCarsCoordinatesLoaded(coordinates) {
    return {
        type: LATEST_CARS_COORDINATES_LOADED,
        coordinates: coordinates
    }
}

export function loadTimeRangeCarTripsCoordinates(carId, start, end) {
    return {
        type: LOAD_TIME_RANGE_CAR_TRIPS_COORDINATES,
        carId,
        start,
        end
    };
}
export function timeRangeCarTripsCoordinatesLoaded(carId, coordinates) {
    return {
        type: TIME_RANGE_CAR_TRIPS_COORDINATES_LOADED,
        carId: carId,
        coordinates: coordinates
    }
}
export function timeRangeCarTripsCoordinatesLoadFailed(carId, reason) {
    return {
        type: TIME_RANGE_CAR_TRIPS_COORDINATES_LOAD_FAILED,
        carId,
        reason
    }
}