//actions
export const LOAD_ADDRESS_BY_COORDINATE = 'LOAD_ADDRESS_BY_COORDINATE';
export const LOADED_ADDRESS_BY_COORDINATE = 'LOADED_ADDRESS_BY_COORDINATE';
export const CLEAR_CAR_GEO_CODE = 'CLEAR_CAR_GEO_CODE';
export const CLEAR_GEO_CODE = 'CLEAR_GEO_CODE';

// action creators
export function loadAddressByCoordinate(coordinate, carId) {
    return {
        type: LOAD_ADDRESS_BY_COORDINATE,
        coordinate,
        carId
    };
}
export function loadedAddressByCoordinate(carId, address, coordinate) {
    return {
        type: LOADED_ADDRESS_BY_COORDINATE,
        carId,
        address,
        coordinate
    }
}
export function clearCarGeoCode(carId) {
    return {
        type: CLEAR_CAR_GEO_CODE,
        carId
    }
}
export function clearGeoCode() {
    return {
        type: CLEAR_GEO_CODE
    }
}
