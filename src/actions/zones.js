//actions
export const LOAD_ZONES = 'LOAD_ZONES';
export const ZONES_LOADED = 'ZONES_LOADED';

export const CREATE_ZONE = 'CREATE_ZONE';
export const CREATED_ZONE = 'CREATED_ZONE';
export const CREATE_ZONE_FAILED = 'CREATE_ZONE_FAILED';

export const UPDATE_ZONE = 'UPDATE_ZONE';
export const UPDATED_ZONE = 'UPDATED_ZONE';
export const UPDATE_ZONE_FAILED = 'UPDATE_ZONE_FAILED';

export const DELETE_ZONE = 'DELETE_ZONE';
export const DELETED_ZONE = 'DELETED_ZONE';
export const DELETE_ZONE_FAILED = 'DELETE_ZONE_FAILED';

// action creators
export function loadZones() {
    return {
        type: LOAD_ZONES
    };
}
export function zonesLoaded(zones) {
    return {
        type: ZONES_LOADED,
        zones
    }
}
export function createZone(zone) {
    return {
        type: CREATE_ZONE,
        zone
    }
}
export function createdZone() {
    return {
        type: CREATED_ZONE
    }
}
export function createZoneFailed(reason) {
    return {
        type: CREATE_ZONE_FAILED,
        reason
    }
}
export function updateZone(id, payload) {
    return {
        type: UPDATE_ZONE,
        id,
        payload
    }
}
export function updatedZone(id) {
    return {
        type: UPDATED_ZONE,
        id
    }
}
export function updateZoneFailed(id, reason) {
    return {
        type: UPDATE_ZONE_FAILED,
        id,
        reason
    }
}
export function deleteZone(id) {
    return {
        type: DELETE_ZONE,
        id
    }
}
export function deletedZone(id) {
    return {
        type: DELETED_ZONE,
        id
    }
}
export function deleteZoneFailed(id, reason) {
    return {
        type: DELETE_ZONE_FAILED,
        id,
        reason
    }
}
