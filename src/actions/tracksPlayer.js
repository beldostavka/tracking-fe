//actions
export const UPDATE_CAR_TRACK_PLAYHEAD = 'UPDATE_CAR_TRACK_PLAYHEAD';
export const CLEAR_CAR_TRACK_PLAYHEAD = 'CLEAR_CAR_TRACK_PLAYHEAD';

// action creators
export function updateCarTrackPlayHead(carId, coordinate) {
    return {
        type: UPDATE_CAR_TRACK_PLAYHEAD,
        carId,
        coordinate
    };
}
export function clearPlayHead(carId) {
    return {
        type: CLEAR_CAR_TRACK_PLAYHEAD,
        carId
    };
}
