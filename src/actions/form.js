import {change} from "redux-form";

// action creators
export function setGeoZone(bounds) {
    return change("geoZone", "bounds", bounds);
}
