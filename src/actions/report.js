export const INITIALIZE_REPORT_FORM = 'INITIALIZE_REPORT_FORM';
export const POPULATE_REPORT_FORM = 'POPULATE_REPORT_FORM';
export const ADD_ITEM_TO_REPORT_FORM = 'ADD_ITEM_TO_REPORT_FORM';

export const GET_REPORT_TRIPS = 'trips';
export const GET_REPORT_GENERIC = 'GET_REPORT_GENERIC';
export const GET_REPORT_SOS = 'sos';
export const GET_REPORT_LOGS = 'logs';

export const TYPE_EVENT_INFO = 'TYPE_EVENT_INFO';
export const TYPE_EVENT_WARNING = 'TYPE_EVENT_WARNING';

export const POPULATE_REPORT_STATE = 'POPULATE_REPORT_STATE';
export const CLEAR_REPORT_STATE = 'CLEAR_REPORT_STATE';


export const EMPTY_REPORT_CURR_DATE_RANGE = 'EMPTY_REPORT_CURR_DATE_RANGE';

export const HOUR_IN_MILISECONDS = 3600000;
export const DAY_IN_MILISECONDS = 86400000;
export const WEEK_IN_MILISECONDS = 604800000;
export const DAY_30_IN_MILISECONDS = 2592000000;

export function initializeReportForm() {
    return {
        type: INITIALIZE_REPORT_FORM,

    }
}

export function addItemToReportForm(itemName, item = null) {
    return {
        type: ADD_ITEM_TO_REPORT_FORM,
        name: itemName,
        payload: item
    }
}

export function populateReportForm(dataForm) {
    return {
        type: POPULATE_REPORT_FORM,
        payload: dataForm
    }
}

export function getReportTrips(id, from, to, report, pause) {
    return {
        type: GET_REPORT_TRIPS,
        id,
        from,
        to,
        report,
        pause
    }

}
export function getReportGeneric(settingsObject) {
    return {
        type: GET_REPORT_GENERIC,
        settingsObject

    }
}

export function pushReportToState(reportContent = []) {
    return {
        type: POPULATE_REPORT_STATE,
        payload: reportContent
    }
}

export function clearReport() {
    return {
        type: CLEAR_REPORT_STATE
    }
}

export function currDateRangeEmptyReport(carId, reason) {
    return {
        type: EMPTY_REPORT_CURR_DATE_RANGE,
        carId,
        reason
    }
}