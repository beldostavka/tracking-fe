//actions
export const SET_ZOOM_CAR = 'SET_ZOOM_CAR';
export const CLEAR_ZOOM = 'CLEAR_ZOOM';

// action creators
export function setZoomCar(carId) {
    return {
        type: SET_ZOOM_CAR,
        carId: carId
    };
}
export function clearZoom() {
    return {
        type: CLEAR_ZOOM
    };
}
