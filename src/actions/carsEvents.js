//actions
export const LOAD_CARS_EVENTS = 'LOAD_CARS_EVENTS';
export const CARS_EVENTS_LOADED = 'CARS_EVENTS_LOADED';

export const CREATE_CAR_EVENT = 'CREATE_CAR_EVENT';
export const CREATED_CAR_EVENT = 'CREATED_CAR_EVENT';
export const CREATE_CAR_EVENT_FAILED = 'CREATE_CAR_EVENT_FAILED';

export const DELETE_CAR_EVENT = 'DELETE_CAR_EVENT';
export const DELETED_CAR_EVENT = 'DELETED_CAR_EVENT';
export const DELETE_CAR_EVENT_FAILED = 'DELETE_CAR_EVENT_FAILED';

// action creators
export function loadCarsEvents() {
    return {
        type: LOAD_CARS_EVENTS
    };
}
export function carsEventsLoaded(carsEvents) {
    return {
        type: CARS_EVENTS_LOADED,
        carsEvents
    }
}
export function createCarEvent(carEvent) {
    return {
        type: CREATE_CAR_EVENT,
        carEvent
    }
}
export function createdCarEvent() {
    return {
        type: CREATED_CAR_EVENT
    }
}
export function createCarEventFailed(reason) {
    return {
        type: CREATE_CAR_EVENT_FAILED,
        reason
    }
}
export function deleteCarEvent(id) {
    return {
        type: DELETE_CAR_EVENT,
        id
    }
}
export function deletedCarEvent(id) {
    return {
        type: DELETED_CAR_EVENT,
        id
    }
}
export function deleteCarEventFailed(id, reason) {
    return {
        type: DELETE_CAR_EVENT_FAILED,
        id,
        reason
    }
}
