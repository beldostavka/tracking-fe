//actions
export const LOAD_GROUPS = 'LOAD_GROUPS';
export const GROUPS_LOADED = 'GROUPS_LOADED';

export const CREATE_GROUP = 'CREATE_GROUP';
export const CREATED_GROUP = 'CREATED_GROUP';
export const CREATE_GROUP_FAILED = 'CREATE_GROUP_FAILED';

export const UPDATE_GROUP = 'UPDATE_GROUP';
export const UPDATED_GROUP = 'UPDATED_GROUP';
export const UPDATE_GROUP_FAILED = 'UPDATE_GROUP_FAILED';

export const DELETE_GROUP = 'DELETE_GROUP';
export const DELETED_GROUP = 'DELETED_GROUP';
export const DELETE_GROUP_FAILED = 'DELETE_GROUP_FAILED';

// action creators
export function loadGroups() {
    return {
        type: LOAD_GROUPS
    };
}
export function groupsLoaded(groups) {
    return {
        type: GROUPS_LOADED,
        groups
    }
}
export function createGroup(group) {
    return {
        type: CREATE_GROUP,
        group
    }
}
export function createdGroup() {
    return {
        type: CREATED_GROUP
    }
}
export function createGroupFailed(reason) {
    return {
        type: CREATE_GROUP_FAILED,
        reason
    }
}
export function updateGroup(id, payload) {
    return {
        type: UPDATE_GROUP,
        id,
        payload
    }
}
export function updatedGroup(id) {
    return {
        type: UPDATED_GROUP,
        id
    }
}
export function updateGroupFailed(id, reason) {
    return {
        type: UPDATE_GROUP_FAILED,
        id,
        reason
    }
}
export function deleteGroup(id) {
    return {
        type: DELETE_GROUP,
        id
    }
}
export function deletedGroup(id) {
    return {
        type: DELETED_GROUP,
        id
    }
}
export function deleteGroupFailed(id, reason) {
    return {
        type: DELETE_GROUP_FAILED,
        id,
        reason
    }
}
