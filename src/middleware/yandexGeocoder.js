import {APP_CONFIG} from "../config/index";
import "whatwg-fetch";
import {LOAD_ADDRESS_BY_COORDINATE, loadedAddressByCoordinate} from "../actions/geocode";

const UNDEFINED_ADDRESS = "Неопознанный адрес";

export const geocoder = store => next => action => {
    switch (action.type) {
        case LOAD_ADDRESS_BY_COORDINATE:
            const coordinate = action.coordinate,
                url = buildUrl(coordinate.latitude, coordinate.longitude);

            console.debug("[Geo code] Prepared url: ", url);

            return fetch(url)
                .then(function (response) {
                    const json = response.json() || {};

                    console.debug("[Geo code] Got response: ", json);

                    return json;
                })
                .then(function (json = {}) {
                    const address = extractAddress(json);

                    console.debug("[Geo code] Got address: ", address);

                    store.dispatch(loadedAddressByCoordinate(
                        action.carId,
                        address,
                        coordinate
                    ));
                })
                .catch(function (e) {
                    console.error('Unable to get address by coordinate', e);

                    store.dispatch(loadedAddressByCoordinate(
                        action.carId,
                        UNDEFINED_ADDRESS,
                        coordinate
                    ));
                });

            break;
        default:
            return next(action);

    }
};

const extractCoordinate = function(json) {
    const geoObjectsArray = json.response && json.response.GeoObjectCollection && Array.isArray(json.response.GeoObjectCollection.featureMember)
            ? json.response.GeoObjectCollection.featureMember
            : [],
        geoObject = geoObjectsArray.pop(),
        positionString = geoObject && geoObject.GeoObject && geoObject.GeoObject.Point && geoObject.GeoObject.Point.pos ? geoObject.GeoObject.Point.pos : "";

    return positionString.split(" ");
};

const extractAddress = function(json) {
    const geoObjectsArray = json.response && json.response.GeoObjectCollection && Array.isArray(json.response.GeoObjectCollection.featureMember)
            ? json.response.GeoObjectCollection.featureMember
            : [],
        geoObject = geoObjectsArray.shift(),
        metadata = geoObject && geoObject.GeoObject && geoObject.GeoObject.metaDataProperty
            && geoObject.GeoObject.metaDataProperty || null;

    return metadata && metadata.GeocoderMetaData && metadata.GeocoderMetaData.text || UNDEFINED_ADDRESS;
};

const buildUrl = (latitude, longitude) => {
    const host = APP_CONFIG.yandexApi.geoCoding.host;

    return `${host}&geocode=${latitude},${longitude}`;
};
