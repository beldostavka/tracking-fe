import {APP_CONFIG} from "../config/index";
import "whatwg-fetch";
import {eventsLoaded, LOAD_EVENTS} from "../actions/events";
import moment from "moment";

export const events = store => next => action => {
    switch (action.type) {
        case LOAD_EVENTS:
            fetch(buildEventsUrl(), {
                credentials: "include"
            })
                .then(function (response) {
                    return response.json() || [];
                })
                .then(function (carsEvents = []) {
                    store.dispatch(eventsLoaded(carsEvents));
                })
                .catch(function (e) {
                    console.error('Unable to load events', e);

                    store.dispatch(eventsLoaded([]));
                });

            break;
        default:
            return next(action);

    }
};

const buildUrl = (path) => {
    const host = APP_CONFIG.application.host;
    const version = APP_CONFIG.application.api.version;
    const apiCommonPath = APP_CONFIG.application.api.path;

    return `${host}${apiCommonPath}/${version}${path}`;
};

const buildEventsUrl = () => {
    const from = moment().startOf('day'),
        to = moment();

    let url = buildUrl(APP_CONFIG.application.api.paths.events);

    return `${url}?from=${from}&to=${to}`
};
