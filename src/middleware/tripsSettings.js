import {APP_CONFIG} from "../config/index";
import "whatwg-fetch";
import {TRIPS_LOADED, tripsBuilt} from "../actions/trips";

export const tripsColors = store => next => action => {
    switch (action.type) {
        case TRIPS_LOADED:
            store.dispatch(tripsBuilt(
                action.carId,
                action.tripsCoordinates,
                APP_CONFIG.trip.settings
            ));

            break;
        default:
            return next(action);

    }
};
