import {
    INITIALIZE_REPORT_FORM,
    GET_REPORT_TRIPS,
    POPULATE_REPORT_FORM,
    DAY_IN_MILISECONDS,
    GET_REPORT_GENERIC,
    GET_REPORT_LOGS,
    GET_REPORT_SOS,
    TYPE_EVENT_INFO,
    TYPE_EVENT_WARNING,
    populateReportForm,
    pushReportToState,
    clearReport,
    currDateRangeEmptyReport


} from '../actions/report';
import {APP_CONFIG} from "../config"

export const report = store => next => action => {
    switch (action.type) {
        case INITIALIZE_REPORT_FORM:
            store.dispatch(populateReportForm(
                {
                    cars: store.cars,
                    typeReports: store.lists,
                    timeRange: DAY_IN_MILISECONDS
                }
                )
            );
            break;

        case GET_REPORT_TRIPS:
            /*fetch(buildReportTripUrl(), {
             credentials: "include"
             }).then(function (response) {
             console.log(response);
             return response.json() || {};
             });
             */
            //console.log(buildReportTripUrl(id, ));
            break;

        case GET_REPORT_GENERIC:
            const settings = action.settingsObject;
            switch (settings.report) {
                case GET_REPORT_TRIPS:
                    fetch(buildReportTripUrl(settings.car, settings.from, settings.to, settings.report, settings.pause), {
                        credentials: "include"
                    }).then(function (response) {
                        return response.json() || [];
                    }).then(function (json) {
                        store.dispatch(clearReport());
                        if (Object.keys(json).length === 0) {

                            store.dispatch(currDateRangeEmptyReport(settings.car, {_error: "Передвижений за выбранную дату не найдено"}));
                        } else {
                            store.dispatch(pushReportToState(json));
                        }
                    }).catch(function (e) {
                        console.error("error while loading report " + settings.report, e)
                        store.dispatch(currDateRangeEmptyReport(settings.car, {_error: "Передвижений за выбранную дату не найдено"}));
                    });
                    break;
                case GET_REPORT_SOS:
                    fetch(buildReportObjectUrl(settings.car, settings.from, settings.to, settings.report), {
                        credentials: "include"
                    }).then(function (response) {
                        return response.json() || [];
                    }).then(function (json) {
                        store.dispatch(clearReport());
                        if (Object.keys(json).length === 0) {

                            store.dispatch(currDateRangeEmptyReport(settings.car, {_error: "Передвижений за выбранную дату не найдено"}));
                        } else {
                            store.dispatch(pushReportToState(json));
                        }
                    }).catch(function (e) {
                        console.error("error while loading report " + settings.report, e)
                        store.dispatch(currDateRangeEmptyReport(settings.car, {_error: "Передвижений за выбранную дату не найдено"}));

                    });
                    break;
                case GET_REPORT_LOGS:
                    fetch(buildReportLogsUrl(settings.car, settings.from, settings.to, settings.report), {
                        credentials: "include"
                    }).then(function (response) {
                        return response.json() || [];
                    }).then(function (json) {

                        store.dispatch(clearReport());
                        if (Object.keys(json).length === 0) {

                            store.dispatch(currDateRangeEmptyReport(settings.car, {_error: "Передвижений за выбранную дату не найдено"}));
                        } else {
                            store.dispatch(pushReportToState(json));
                        }
                    }).catch(function (e) {
                        console.error("error while loading report " + settings.report, e)
                        store.dispatch(currDateRangeEmptyReport(settings.car, {_error: "Передвижений за выбранную дату не найдено"}));
                    });
                    break;
            }


            break;

        default:
            return next(action);
    }
};

const buildUrl = (path, report) => {
    const host = APP_CONFIG.application.host;
    const version = APP_CONFIG.application.api.version;
    const apiCommonPath = APP_CONFIG.application.api.path;
    return `${host}${apiCommonPath}/${version}${path}/${report}`;
};


const buildReportForObjectUrl = (id, report) => {
    return buildUrl(APP_CONFIG.application.api.paths.reportsObject, report)
        .replace("{id}", id);
};


const buildReportObjectUrl = (id, from, to, report) => {
    const uri = buildReportForObjectUrl(id, report);
    return `${uri}?from=${from}&to=${to}`;
};

const buildReportTripUrl = (id, from, to, report, pause = 20) => {
    const uri = buildReportObjectUrl(id, from, to, report);
    return `${uri}&pause=${pause}`;
};

const buildReportLogsUrl = (id, from, to, report, type = TYPE_EVENT_WARNING) => {
    const uri = buildReportObjectUrl(id, from, to, report);
    return `${uri}&type=${type}`;
};


