import {stopSubmit} from "redux-form";
import {CAR_UPDATE_FAILED, TIME_RANGE_CAR_TRIPS_COORDINATES_LOAD_FAILED} from "../actions/cars";
import {CREATE_GROUP_FAILED, DELETE_GROUP_FAILED, UPDATE_GROUP_FAILED} from "../actions/groups";
import {DELETE_ZONE_FAILED, UPDATE_ZONE_FAILED} from "../actions/zones";

export const form = store => next => action => {
    const error = action.reason,
        id = action.id;

    switch (action.type) {
        case CAR_UPDATE_FAILED:
            store.dispatch(stopSubmit("carSettings", error));

            break;
        case TIME_RANGE_CAR_TRIPS_COORDINATES_LOAD_FAILED:
            store.dispatch(stopSubmit("carTrips", error));

            break;
        case CREATE_GROUP_FAILED:
            store.dispatch(stopSubmit(`groupSettings`, error));

            break;
        case UPDATE_GROUP_FAILED:
            store.dispatch(stopSubmit(`groupSettings-${id}`, error));

            break;
        case DELETE_GROUP_FAILED:
            store.dispatch(stopSubmit(`groupSettings-${id}`, error));

            break;
        case UPDATE_ZONE_FAILED:
            store.dispatch(stopSubmit(`zoneSettings-${id}`, error));

            break;
        case DELETE_ZONE_FAILED:
            store.dispatch(stopSubmit(`zoneSettings-${id}`, error));

            break;
        default:
            return next(action);

    }
};
