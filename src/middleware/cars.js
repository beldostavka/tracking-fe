import {
    latestCarsCoordinatesLoaded, carsLoaded, LOAD_CARS, LOAD_LATEST_CARS_COORDINATES,
    LOAD_TIME_RANGE_CAR_TRIPS_COORDINATES,
    timeRangeCarTripsCoordinatesLoaded, UPDATE_CAR, LOAD_CAR, loadCar, carLoaded, CAR_UPDATED, carUpdated,
    carUpdateFailed, timeRangeCarTripsCoordinatesLoadFailed, UPDATE_CARS, carsUpdateFailed, carsUpdated, CARS_UPDATED,
    loadCars, TIME_RANGE_CAR_TRIPS_COORDINATES_LOAD_FAILED,
} from "../actions/cars";
import {APP_CONFIG} from "../config/index";
import "whatwg-fetch";
import i18n from '../i18n';

export const cars = store => next => action => {
    let carId,
        start,
        end,
        payload,
        body;

    switch (action.type) {
        case LOAD_CARS:
            fetch(buildGetCarsUrl(), {
                credentials: "include"
            })
                .then(function (response) {
                    return response.json() || [];
                })
                .then(function (carsList = []) {
                    store.dispatch(carsLoaded(carsList));
                })
                .catch(function (e) {
                    console.error('Unable to load cars list', e);

                    store.dispatch(carsLoaded([]));
                });

            break;
        case LOAD_LATEST_CARS_COORDINATES:
            fetch(buildGetLatestCarsCoordinatesUrl(), {
                credentials: "include"
            })
                .then(function (response) {
                    return response.json() || {};
                })
                .then(function (coordinates = {}) {
                    store.dispatch(latestCarsCoordinatesLoaded(coordinates));
                })
                .catch(function (e) {
                    console.error('Unable to load cars coordinates', e);

                    store.dispatch(latestCarsCoordinatesLoaded({}));
                });

            break;
        case LOAD_TIME_RANGE_CAR_TRIPS_COORDINATES:
            carId = action.carId;
            start = action.start;
            end = action.end;

            fetch(buildGetTimeRangeCarTripsCoordinatesUrl(carId, start, end), {
                credentials: "include"
            })
                .then(function (response) {
                    return response.json() || {};
                })
                .then(function (coordinates = {}) {
                    store.dispatch(timeRangeCarTripsCoordinatesLoaded(carId, coordinates));
                })
                .catch(function (e) {
                    console.error(`Unable to load car #${carId} trips coordinates for the provided range: start = ${start}, end = ${end}`, e);

                    store.dispatch(timeRangeCarTripsCoordinatesLoadFailed(carId, {_error: i18n.t('trips:no_trips_for_date')}));
                });

            break;
        case TIME_RANGE_CAR_TRIPS_COORDINATES_LOAD_FAILED:
            store.dispatch(timeRangeCarTripsCoordinatesLoaded(action.carId, {}));

            return next(action);
            break;
        case UPDATE_CAR:
            carId = action.carId;
            payload = action.payload;

            body = JSON.stringify(payload);

            fetch(buildPatchCarUrl(carId), {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: body,
                credentials: "include"
            })
                .then(response => {
                    if (response.status >= 200 && response.status < 400) {
                        store.dispatch(carUpdated(carId));
                    } else {
                        return response.json();
                    }
                })
                .then(error => {
                    store.dispatch(carUpdateFailed(carId, error));
                })
                .catch(reason => {
                    console.error(`Unable to update car #${carId} with the following payload: ${body}`, reason);

                    store.dispatch(carUpdateFailed(carId, {_error: i18n.t('forms:could_not_save')}));
                });

            break;
        case UPDATE_CARS:
            payload = action.payload;

            body = JSON.stringify(payload);

            fetch(buildPatchCarsUrl(), {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: body,
                credentials: "include"
            })
                .then(response => {
                    if (response.status >= 200 && response.status < 400) {
                        store.dispatch(carsUpdated());
                    } else {
                        return response.json();
                    }
                })
                .then(error => {
                    store.dispatch(carsUpdateFailed(error));
                })
                .catch(reason => {
                    console.error(`Unable to update cars with the following payload: ${body}`, reason);

                    store.dispatch(carsUpdateFailed({_error: i18n.t('forms:could_not_save')}));
                });

            break;
        case CAR_UPDATED:
            store.dispatch(loadCar(action.carId));

            break;
        case CARS_UPDATED:
            store.dispatch(loadCars());

            break;
        case LOAD_CAR:
            carId = action.carId;

            fetch(buildGetCarUrl(carId), {
                credentials: "include"
            })
                .then(function (response) {
                    return response.json() || null;
                })
                .then(function (car = null) {
                    store.dispatch(carLoaded(car));
                })
                .catch(function (e) {
                    console.error(`Unable to load car #${carId}`, e);

                    store.dispatch(carLoaded(null));
                });
            break;
        default:
            return next(action);

    }
};

const buildUrl = (path) => {
    const host = APP_CONFIG.application.host;
    const version = APP_CONFIG.application.api.version;
    const apiCommonPath = APP_CONFIG.application.api.path;

    return `${host}${apiCommonPath}/${version}${path}`;
};

const buildGetCarsUrl = () => {
    return buildUrl(APP_CONFIG.application.api.paths.objectsList);
};

const buildGetCarUrl = id => {
    return buildUrl(APP_CONFIG.application.api.paths.object)
        .replace("{id}", id);
};

const buildPatchCarUrl = id => {
    return buildUrl(APP_CONFIG.application.api.paths.object)
        .replace("{id}", id);
};

const buildPatchCarsUrl = () => {
    return buildUrl(APP_CONFIG.application.api.paths.objectsList);
};

const buildGetLatestCarsCoordinatesUrl = () => {
    const uri = buildUrl(APP_CONFIG.application.api.paths.objectsTracks);

    return `${uri}?count=1`;
};

const buildGetTimeRangeCarTripsCoordinatesUrl = (id, from, to) => {
    const pause = APP_CONFIG.trip.pause;
    const uri = buildUrl(APP_CONFIG.application.api.paths.objectTrips)
        .replace("{id}", id);

    return `${uri}?from=${from}&to=${to}&pause=${pause}`;
};
