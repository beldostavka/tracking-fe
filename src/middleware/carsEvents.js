import {APP_CONFIG} from "../config/index";
import "whatwg-fetch";
import {
    carsEventsLoaded, CREATE_CAR_EVENT, createCarEventFailed, CREATED_CAR_EVENT, createdCarEvent, DELETE_CAR_EVENT,
    deleteCarEventFailed,
    DELETED_CAR_EVENT,
    deletedCarEvent,
    LOAD_CARS_EVENTS,
    loadCarsEvents
} from "../actions/carsEvents";
import i18n from '../i18n';

export const carsEvents = store => next => action => {
    let body,
        id;

    switch (action.type) {
        case LOAD_CARS_EVENTS:
            fetch(buildCarsEventsUrl(), {
                credentials: "include"
            })
                .then(function (response) {
                    return response.json() || [];
                })
                .then(function (carsEvents = []) {
                    store.dispatch(carsEventsLoaded(carsEvents));
                })
                .catch(function (e) {
                    console.error('Unable to load cars events', e);

                    store.dispatch(carsEventsLoaded([]));
                });

            break;
        case CREATE_CAR_EVENT:
            body = JSON.stringify(action.carEvent);

            fetch(buildCarsEventsUrl(), {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: body,
                credentials: "include"
            })
                .then(response => {
                    if (response.status >= 200 && response.status < 400) {
                        store.dispatch(createdCarEvent());
                    } else {
                        return response.json();
                    }
                })
                .then(error => {
                    store.dispatch(createCarEventFailed(error));
                })
                .catch(reason => {
                    console.error(`Unable to create car event with the following data: ${body}`, reason);

                    store.dispatch(createCarEventFailed({_error: i18n.t('events:could_not_create')}));
                });

            break;
        case DELETE_CAR_EVENT:
            id = action.id;

            fetch(buildCarEventUrl(id), {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: body,
                credentials: "include"
            })
                .then(response => {
                    if (response.status >= 200 && response.status < 400) {
                        store.dispatch(deletedCarEvent(id));
                    } else {
                        let error = new Error(response.statusText);

                        error.response = response;

                        throw error;
                    }
                })
                .catch(reason => {
                    console.error(`Unable to delete car event`, reason);

                    store.dispatch(deleteCarEventFailed(id, {_error: i18n.t('events:could_not_delete')}));
                });

            break;

        case CREATED_CAR_EVENT:
        case DELETED_CAR_EVENT:
            store.dispatch(loadCarsEvents());

            return next(action);

            break;
        default:
            return next(action);

    }
};

const buildUrl = (path) => {
    const host = APP_CONFIG.application.host;
    const version = APP_CONFIG.application.api.version;
    const apiCommonPath = APP_CONFIG.application.api.path;

    return `${host}${apiCommonPath}/${version}${path}`;
};

const buildCarsEventsUrl = () => {
    return buildUrl(APP_CONFIG.application.api.paths.carsEvents);
};

const buildCarEventUrl = (id) => {
    return buildUrl(APP_CONFIG.application.api.paths.carEvent)
        .replace("{id}", id);
};
