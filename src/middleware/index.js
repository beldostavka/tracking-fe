import {applyMiddleware, compose} from 'redux';
import {cars} from "./cars";
import {profile} from "./profile";
import {trips} from "./googleTrips";
import {geocoder} from "./yandexGeocoder";
import {tripsColors} from "./tripsSettings";
import {monitoring} from "./monitoring";
import {form} from "./form";
import {groups} from "./groups";
import {lists} from "./lists";
import {zones} from "./zones";
import {carsEvents} from "./carsEvents";
import {events} from "./events";
import {report} from './report';
//import {createLogger} from 'redux-logger'
import {logger} from 'redux-logger'


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const middlewares = composeEnhancers(
    applyMiddleware(
        cars,
        // Use concrete implementation of router, it might be google/yandex/osm
        trips,
        tripsColors,
        // Use concrete implementation of geoCoder, it might be google/yandex/osm
        geocoder,
        profile,
        monitoring,
        groups,
        form,
        lists,
        zones,
        carsEvents,
        events,
        report,
        logger
    )
);
