import {APP_CONFIG} from "../config/index";
import "whatwg-fetch";
import {TIME_RANGE_CAR_TRIPS_COORDINATES_LOADED} from "../actions/cars";
import {BUILD_TRIPS, buildTrips, tripsBuilt, tripsLoaded} from "../actions/trips";

export const trips = store => next => action => {
    let tripIndex,
        promises = [],
        coordinates;

    switch (action.type) {
        case BUILD_TRIPS:
            const tripsCoordinates = action.tripsCoordinates || {};
            let tripCoordinates;

            for (tripIndex in tripsCoordinates) {
                if (!tripsCoordinates.hasOwnProperty(tripIndex)) {
                    continue;
                }

                tripCoordinates = tripsCoordinates[tripIndex];

                coordinates = Array.isArray(tripCoordinates) && tripCoordinates || [];

                promises.push(mapReduceCoordinatesToRoad(coordinates));
            }

            Promise
                .all(promises)
                .then(snappedTripsCoordinates => {
                    store.dispatch(tripsLoaded(action.carId, snappedTripsCoordinates));
                }, (error) => {
                    console.error("Unable to build trips, reason: ", error);

                    store.dispatch(tripsLoaded(action.carId, null));
                });

            break;
        case TIME_RANGE_CAR_TRIPS_COORDINATES_LOADED:
            store.dispatch(buildTrips(action.carId, action.coordinates));

            break;
        default:
            return next(action);
    }
};

const mapReduceCoordinatesToRoad = (coordinates) => {
    const coordinatesPortionLimit = APP_CONFIG.googleApi.roads.maxCoordinatesToSnap;

    let index,
        promises = [],
        length = coordinates.length;

    for (index = 0; index < length; index += coordinatesPortionLimit) {
        promises.push(snapToRoad(
            coordinates.slice(index, index + coordinatesPortionLimit)
        ));
    }

    return new Promise((resolve, reject) => {
        Promise
            .all(promises)
            .then(snappedCoordinates => {
                resolve([].concat.apply([], snappedCoordinates));
            }, (error) => {
                console.error("Unable to map-reduce coordinates, reason: ", error);

                reject([]);
            });
    });
};

const snapToRoad = (coordinates) => {
    const url = buildUrl(coordinates);

    console.info("[Snap to road] Prepared url: ", url);

    return fetch(url)
        .then(function (response) {
            console.info("[Snap to road] Got response: ", response);

            return response.json() || {};
        })
        .then(function (coordinatesHash = {}) {
            const snappedCoordinates = coordinatesHash.snappedPoints;

            if (!Array.isArray(snappedCoordinates)) {
                throw new Error('Google didn\'t return array of snapped points');
            }

            if (coordinatesHash.warningMessage) {
                console.error(coordinatesHash.warningMessage);
            }

            return Promise.resolve(
                combineOriginalAndSnappedCoordinates(coordinates, snappedCoordinates)
            );
        })
        .catch(function (e) {
            console.error('Unable to get snapped points to road from google', e);

            return Promise.resolve([]);
        });
};

const buildUrl = (coordinates = []) => {
    const host = APP_CONFIG.googleApi.roads.host;
    const path = coordinates.map(coordinate => {
        const lat = coordinate.lattitude || "";
        const lng = coordinate.longitude || "";

        return `${lat},${lng}`;
    })
        .join("|");

    return `${host}&path=${path}`;
};

const combineOriginalAndSnappedCoordinates = (originalCoordinates, snappedCoordinates) => {
    let originalIndex,
        lastOriginalIndex;

    snappedCoordinates.forEach((snappedCoordinate) => {
        originalIndex = snappedCoordinate.originalIndex;

        if (Number.isInteger(originalIndex)) {
            lastOriginalIndex = originalIndex;

            snappedCoordinate.source = originalCoordinates[originalIndex];
        } else if (Number.isInteger(lastOriginalIndex)) {
            snappedCoordinate.source = originalCoordinates[lastOriginalIndex];
        }
    });

    return snappedCoordinates;
};
