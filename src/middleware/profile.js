import {APP_CONFIG} from "../config/index";
import "whatwg-fetch";
import {LOAD_PROFILE, profileLoaded} from "../actions/profile";

export const profile = store => next => action => {
    switch (action.type) {
        case LOAD_PROFILE:
            fetch(buildLoadProfileUrl(), {
                credentials: "include"
            })
                .then(function (response) {
                    return response.json() || {};
                })
                .then(function (profile = {}) {
                    store.dispatch(profileLoaded(profile));
                })
                .catch(function (e) {
                    console.error('Unable to load profile', e);

                    store.dispatch(profileLoaded(null));
                });

            break;
        default:
            return next(action);

    }
};

const buildUrl = (path) => {
    const host = APP_CONFIG.application.host;
    const version = APP_CONFIG.application.api.version;
    const apiCommonPath = APP_CONFIG.application.api.path;

    return `${host}${apiCommonPath}/${version}${path}`;
};

const buildLoadProfileUrl = () => {
    return buildUrl(APP_CONFIG.application.api.paths.profile);
};
