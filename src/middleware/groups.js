import {APP_CONFIG} from "../config/index";
import "whatwg-fetch";
import {
    CREATE_GROUP, CREATED_GROUP, createdGroup, createGroupFailed, DELETE_GROUP, DELETED_GROUP, deletedGroup,
    deleteGroupFailed,
    groupsLoaded, LOAD_GROUPS,
    loadGroups,
    UPDATE_GROUP, UPDATED_GROUP, updatedGroup, updateGroupFailed
} from "../actions/groups";

export const groups = store => next => action => {
    let body,
        id;

    switch (action.type) {
        case LOAD_GROUPS:
            fetch(buildGroupsListUrl(), {
                credentials: "include"
            })
                .then(function (response) {
                    return response.json() || [];
                })
                .then(function (groups = []) {
                    store.dispatch(groupsLoaded(groups));
                })
                .catch(function (e) {
                    console.error('Unable to load groups', e);

                    store.dispatch(groupsLoaded(null));
                });

            break;
        case CREATE_GROUP:
            body = JSON.stringify(action.group);

            fetch(buildGroupsListUrl(), {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: body,
                credentials: "include"
            })
                .then(response => {
                    if (response.status >= 200 && response.status < 400) {
                        store.dispatch(createdGroup());
                    } else {
                        return response.json();
                    }
                })
                .then(error => {
                    store.dispatch(createGroupFailed(error));
                })
                .catch(reason => {
                    console.error(`Unable to create group with the following data: ${body}`, reason);

                    store.dispatch(createGroupFailed({_error: "Не удалось создать группу, проверьте данные формы"}));
                });

            break;
        case UPDATE_GROUP:
            body = JSON.stringify(action.payload);
            id = action.id;

            fetch(buildGroupUrl(id), {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: body,
                credentials: "include"
            })
                .then(response => {
                    if (response.status >= 200 && response.status < 400) {
                        store.dispatch(updatedGroup(id));
                    } else {
                        return response.json();
                    }
                })
                .then(error => {
                    store.dispatch(updateGroupFailed(id, error));
                })
                .catch(reason => {
                    console.error(`Unable to update group with the following data: ${body}`, reason);

                    store.dispatch(updateGroupFailed(id, {_error: "Не удалось обновить группу, проверьте данные формы"}));
                });

            break;
        case DELETE_GROUP:
            id = action.id;

            fetch(buildGroupUrl(id), {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: body,
                credentials: "include"
            })
                .then(response => {
                    if (response.status >= 200 && response.status < 400) {
                        store.dispatch(deletedGroup(id));
                    } else {
                        let error = new Error(response.statusText);

                        error.response = response;

                        throw error;
                    }
                })
                .catch(reason => {
                    console.error(`Unable to delete group`, reason);

                    store.dispatch(deleteGroupFailed(id, {_error: "Не удалось удалить группу"}));
                });

            break;

        case CREATED_GROUP:
        case UPDATED_GROUP:
        case DELETED_GROUP:
            store.dispatch(loadGroups());

            return next(action);

            break;
        default:
            return next(action);

    }
};

const buildUrl = (path) => {
    const host = APP_CONFIG.application.host;
    const version = APP_CONFIG.application.api.version;
    const apiCommonPath = APP_CONFIG.application.api.path;

    return `${host}${apiCommonPath}/${version}${path}`;
};

const buildGroupsListUrl = () => {
    return buildUrl(APP_CONFIG.application.api.paths.groupsList);
};

const buildGroupUrl = (id) => {
    return buildUrl(APP_CONFIG.application.api.paths.group)
        .replace("{id}", id);
};
