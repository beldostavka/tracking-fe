import {APP_CONFIG} from "../config/index";
import "whatwg-fetch";
import {
    CREATE_ZONE, CREATED_ZONE, createdZone, createZoneFailed, DELETE_ZONE, DELETED_ZONE, deletedZone, deleteZoneFailed,
    LOAD_ZONES,
    loadZones,
    UPDATE_ZONE, UPDATED_ZONE,
    updatedZone,
    updateZoneFailed,
    zonesLoaded
} from "../actions/zones";

export const zones = store => next => action => {
    let body,
        id;

    switch (action.type) {
        case LOAD_ZONES:
            fetch(buildZonesListUrl(), {
                credentials: "include"
            })
                .then(function (response) {
                    return response.json() || [];
                })
                .then(function (zones = []) {
                    store.dispatch(zonesLoaded(zones));
                })
                .catch(function (e) {
                    console.error('Unable to load zones', e);

                    store.dispatch(zonesLoaded(null));
                });

            break;
        case CREATE_ZONE:
            body = JSON.stringify(action.zone);

            fetch(buildZonesListUrl(), {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: body,
                credentials: "include"
            })
                .then(response => {
                    if (response.status >= 200 && response.status < 400) {
                        store.dispatch(createdZone());
                    } else {
                        return response.json();
                    }
                })
                .then(error => {
                    store.dispatch(createZoneFailed(error));
                })
                .catch(reason => {
                    console.error(`Unable to create zone with the following data: ${body}`, reason);

                    store.dispatch(createZoneFailed({_error: "Не удалось создать зону, проверьте данные формы"}));
                });

            break;
        case UPDATE_ZONE:
            body = JSON.stringify(action.payload);
            id = action.id;

            fetch(buildZoneUrl(id), {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: body,
                credentials: "include"
            })
                .then(response => {
                    if (response.status >= 200 && response.status < 400) {
                        store.dispatch(updatedZone(id));
                    } else {
                        return response.json();
                    }
                })
                .then(error => {
                    store.dispatch(updateZoneFailed(id, error));
                })
                .catch(reason => {
                    console.error(`Unable to update zone with the following data: ${body}`, reason);

                    store.dispatch(updateZoneFailed(id, {_error: "Не удалось обновить зону, проверьте данные формы"}));
                });

            break;
        case DELETE_ZONE:
            id = action.id;

            fetch(buildZoneUrl(id), {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: body,
                credentials: "include"
            })
                .then(response => {
                    if (response.status >= 200 && response.status < 400) {
                        store.dispatch(deletedZone(id));
                    } else {
                        let error = new Error(response.statusText);

                        error.response = response;

                        throw error;
                    }
                })
                .catch(reason => {
                    console.error(`Unable to delete zone`, reason);

                    store.dispatch(deleteZoneFailed(id, {_error: "Не удалось удалить зону"}));
                });

            break;

        case CREATED_ZONE:
        case UPDATED_ZONE:
        case DELETED_ZONE:
            store.dispatch(loadZones());

            return next(action);

            break;
        default:
            return next(action);

    }
};

const buildUrl = (path) => {
    const host = APP_CONFIG.application.host;
    const version = APP_CONFIG.application.api.version;
    const apiCommonPath = APP_CONFIG.application.api.path;

    return `${host}${apiCommonPath}/${version}${path}`;
};

const buildZonesListUrl = () => {
    return buildUrl(APP_CONFIG.application.api.paths.zonesList);
};

const buildZoneUrl = (id) => {
    return buildUrl(APP_CONFIG.application.api.paths.zone)
        .replace("{id}", id);
};
