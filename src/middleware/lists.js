import {APP_CONFIG} from "../config/index";
import "whatwg-fetch";
import {listsLoaded, LOAD_LISTS} from "../actions/lists";

export const lists = store => next => action => {
    switch (action.type) {
        case LOAD_LISTS:
            fetch(buildLoadListsUrl(), {
                credentials: "include"
            })
                .then(function (response) {
                    if (response.ok) {
                        return response.json();
                    } else {
                        let error = new Error('Error occurred while loading lists');
                        error.response = response;
                        throw error;
                    }
                })
                .then(function (lists = {}) {
                    store.dispatch(listsLoaded(lists));
                })
                .catch(function (e) {
                    console.error('Unable to load lists', e);

                    store.dispatch(listsLoaded({}));
                });

            break;
        default:
            return next(action);

    }
};

const buildUrl = (path) => {
    const host = APP_CONFIG.application.host;
    const version = APP_CONFIG.application.api.version;
    const apiCommonPath = APP_CONFIG.application.api.path;

    return `${host}${apiCommonPath}/${version}${path}`;
};

const buildLoadListsUrl = () => {
    return buildUrl(APP_CONFIG.application.api.paths.lists);
};
