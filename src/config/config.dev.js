export const APP_CONFIG = {
    production: false,

    googleApi: {
        roads: {
            host: "https://roads.googleapis.com/v1/snapToRoads?interpolate=true&key=AIzaSyA4t2WvheRQuKC5O0bdCdyOAz0o0iJSfdc",
            maxCoordinatesToSnap: 100
        }
    },
    googleMap: {
        apiKey: "AIzaSyBudUCMpobqNkJIHKgalqMhE68o9CmqGL4"
    },
    yandexApi: {
        geoCoding: {
            host: "https://geocode-maps.yandex.ru/1.x/?format=json&sco=latlong"
        }
    },
    application: {
        host: "http://dev.monitoring.beldostavka.by:8080/webapi-core",
        urls: {
            logout: "/logout"
        },
        api: {
            version: "v1",
            path: "/rc/fe",
            paths: {
                objectsList: "/objects",
                object: "/objects/{id}",

                objectsTracks: "/objects/tracks",
                objectTracks: "/objects/{id}/tracks",
                objectTrips: "/objects/{id}/trips",

                carsEvents: "/etv",
                carEvent: "/etv/{id}",

                events: "/events",

                zonesList: "/geozones",
                zone: "/geozones/{id}",

                groupsList: "/groups",
                group: "/groups/{id}",

                profile: "/profile",

                lists: "/lists",

                reports: "/reports",
                reportsObject: "/reports/objects/{id}",
                reportsGroups: "/reports/groups/{id}"
            },
        }
    },
    monitoring: {
        updateFrequency: 1000 * 1000,
        defaultZoom: 12,
        maxZoom: 19,
        scaleZoomRatio: {
            19: 1,
            18: 1,
            17: 1,
            16: 0.9,
            15: 0.85,
            14: 0.8,
            13: 0.75,
            12: 0.7,
            11: 0.65,
            10: 0.6,
            9: 0.55,
            8: 0.5,
            7: 0.5,
            6: 0.4,
            5: 0.4,
            4: 0.4,
            3: 0.4,
            2: 0.4,
            1: 0.4,
        }
    },
    trip: {
        filter: {
            maxRangeLimit: {
                days: 2
            }
        },
        pause: 30,   // minutes,
        settings: {
            strokeWeight: 5
        }
    },
    events: {
        updateFrequency: 60 * 1000,
    },
    player: {
        speed: 1000
    },
    report: {
        maxRangeLimit:{
            'days':10
        }
    },
    time: {
        format: "dddd, Do MMMM YYYY, kk:mm:ss",
        formatFilter: "Do MMM YYYY, kk:mm:ss"
    },
    defaultLocale: "ru"
};
