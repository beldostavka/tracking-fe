import {APP_CONFIG as APP_CONFIG_DEV}  from "./config.dev";
import {APP_CONFIG as APP_CONFIG_PROD}  from "./config.prod";

// let config = {};
//
// if (process.env.NODE_ENV === 'production') {
//     config = APP_CONFIG_PROD;
// } else {
//     config = APP_CONFIG_DEV;
// }

export const APP_CONFIG = APP_CONFIG_PROD;
